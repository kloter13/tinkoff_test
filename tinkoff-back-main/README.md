This project has JWT, migrations, user and role entities and web-project structure

#### First you need to create *.env_web_db* file and copy data from *.env_web_db.example* to the new file, then change POSTGRES password from the one

### Local build project
```bash
make local_build
```

### Local run project
```bash
make local_up
```

### Remove the containers
```bash
make local_down
```