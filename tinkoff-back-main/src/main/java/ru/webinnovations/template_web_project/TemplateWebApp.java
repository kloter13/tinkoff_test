package ru.webinnovations.template_web_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@EnableScheduling
@EnableSwagger2
//@EnableConfigurationProperties
public class TemplateWebApp {

	public static void main(String[] args) {
		SpringApplication.run(TemplateWebApp.class, args);
	}

}
