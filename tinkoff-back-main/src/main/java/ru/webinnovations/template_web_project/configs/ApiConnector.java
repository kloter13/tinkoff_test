package ru.webinnovations.template_web_project.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import ru.tinkoff.piapi.core.InvestApi;
import ru.webinnovations.template_web_project.db.models.User;

@Slf4j
@Component
public class ApiConnector implements AutoCloseable {

    private Parameters parameters;
    private InvestApi investApi;

    @Autowired
    public ApiConnector(Parameters parameters) {
        this.parameters = parameters;
    }

    public ApiConnector() {
    }

    public InvestApi getInvestApi() throws Exception {
        if (investApi == null) {
            close();
            if (parameters.getToken().isBlank()) {
                log.info("bad or empty tinkoff token");
            } else {
                log.info("Create TINKOFF INVEST API connection");
                investApi = InvestApi.create(parameters.getToken());
            }
        }
        return investApi;
    }


    public InvestApi getInvestApi(String token) throws Exception {
        if (investApi == null) {
            close();
            log.info("Create TINKOFF INVEST API connection");
            if (parameters.isSandBoxMode()) {
                investApi = InvestApi.createSandbox(token);
            } else {
                investApi = InvestApi.create(token);
            }
        }
        return investApi;
    }

    public InvestApi getInvestApi(User user) {
        if (investApi == null) {
            if (user.getIsTokenCreated() != null) {
                if (user.getIsTokenCreated()) {
                    log.info("Create TINKOFF INVEST API connection");
                    this.parameters.setToken(user.getToken());
                    investApi = InvestApi.create(user.getToken());
                } else {
                    throw new RuntimeException("Bad tinkoff api token");
                }
            } else {
                throw new RuntimeException("Bad tinkoff api token");
            }
        }
        return investApi;
    }


    public void resetTinkoffAPI() {
        investApi = null;
    }

    @Override
    public void close() throws Exception {

    }
}
