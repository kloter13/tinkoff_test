package ru.webinnovations.template_web_project.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tinkoff.piapi.core.InvestApi;
import ru.webinnovations.template_web_project.db.models.User;

@Slf4j
@Component
public class ContextProvider {

    private ApiConnector apiConnector;

    @Autowired
    public ContextProvider(ApiConnector apiConnector) {
        this.apiConnector = apiConnector;
    }

    public InvestApi getInvestApi() throws Exception {
        return apiConnector.getInvestApi();
    }

    public InvestApi getInvestApi(String token) throws Exception {
        return apiConnector.getInvestApi(token);
    }

    public InvestApi getInvestApi(User user)  {
        return apiConnector.getInvestApi(user);
    }

    public void resetInvestApi(){
        apiConnector.resetTinkoffAPI();
    }
}
