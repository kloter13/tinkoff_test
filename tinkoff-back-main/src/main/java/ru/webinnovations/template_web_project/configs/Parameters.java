package ru.webinnovations.template_web_project.configs;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class Parameters {

    //@Value(value = "${tinkoff.token}")
    private String token ="";
    //@Value(value = "${tinkoff.isSandboxMode}")
    private boolean isSandBoxMode;

    public Parameters(String token, boolean isSandBoxMode) {
        setParameters(token, isSandBoxMode);
    }

    public Parameters() {
    }

    private void setParameters(String token, boolean sandBoxMode) {
        this.token = token;
        this.isSandBoxMode = sandBoxMode;
    }

    public void setToken(String token){
        this.token = token;
    }

    @Override
    public final String toString() {
        return String.format("core.Parameters: sandBoxMode = %s", isSandBoxMode ? "true" : "false");
    }
}
