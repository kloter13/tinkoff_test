package ru.webinnovations.template_web_project.db.enums;

public enum InstrumentTypeEnum {

    BOND,
    SHARE,
    ETF,
    FUTURE,
    OPTION,
    CURRENCY
}
