package ru.webinnovations.template_web_project.db.enums;

public enum OrderTypeEnum {
    ORDER_TYPE_MARKET,
    ORDER_TYPE_BESTPRICE,
    ORDER_TYPE_LIMIT,
    ORDER_TYPE_UNSPECIFIED
}
