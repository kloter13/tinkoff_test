package ru.webinnovations.template_web_project.db.enums;

public enum StrategyExecutionStatus {

    SUCCESS,
    IN_PROGRESS,
    PARTLY,
    EXPIRED,
    FAILURE
}
