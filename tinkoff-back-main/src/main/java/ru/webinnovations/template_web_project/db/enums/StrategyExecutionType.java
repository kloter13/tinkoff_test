package ru.webinnovations.template_web_project.db.enums;

public enum StrategyExecutionType {

    BUY,
    SELL
}
