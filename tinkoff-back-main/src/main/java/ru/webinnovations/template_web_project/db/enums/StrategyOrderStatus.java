package ru.webinnovations.template_web_project.db.enums;

public enum StrategyOrderStatus {

    //none
    EXECUTION_REPORT_STATUS_UNSPECIFIED,
    //Исполнена
    EXECUTION_REPORT_STATUS_FILL,
    //Отклонена
    EXECUTION_REPORT_STATUS_REJECTED,
    //Отменена пользователем
    EXECUTION_REPORT_STATUS_CANCELLED,
    //Новая
    EXECUTION_REPORT_STATUS_NEW,
    //Частично исполнена
    EXECUTION_REPORT_STATUS_PARTIALLYFILL
}
