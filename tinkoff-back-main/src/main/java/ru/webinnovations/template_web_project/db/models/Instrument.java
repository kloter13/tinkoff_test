package ru.webinnovations.template_web_project.db.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "instrument")
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class Instrument {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private InstrumentTypeEnum type;

    private String uid;

    @Column(name = "position_uid")
    private String positionUid;

    private String figi;

    private String ticker;

    private String isin;

    /**
     * Лотность
     */
    private String lot;

    /**
     * Валюта инструмента
     */
    private String currency;

    @Column(name = "nominal_in_currency")
    private String nominalInCurrency;

    /**
     * Текущая котировка ценной бумаги
     */
    private String price;

    /**
     * Цена последней следки
     */
    @Column(name = "last_price")
    private String lastPrice;

    /**
     * Номинал
     */
    private String nominal;

    /**
     * Шаг цены
     */
    @Column(name = "min_price_increment")
    private String minPriceIncrement;

    /**
     * Стоимость шага цены
     */
    @Column(name = "min_price_increment_amount")
    private String minPriceIncrementAmount;

    @Transient
    private Boolean isAvailable;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrument that = (Instrument) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name)
                && type == that.type && Objects.equals(uid, that.uid) && Objects.equals(positionUid, that.positionUid)
                && Objects.equals(figi, that.figi) && Objects.equals(ticker, that.ticker) && Objects.equals(isin, that.isin)
                && Objects.equals(lot, that.lot) && Objects.equals(currency, that.currency) && Objects.equals(nominalInCurrency, that.nominalInCurrency)
                && Objects.equals(price, that.price) && Objects.equals(lastPrice, that.lastPrice) && Objects.equals(nominal, that.nominal)
                && Objects.equals(minPriceIncrement, that.minPriceIncrement) && Objects.equals(minPriceIncrementAmount, that.minPriceIncrementAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, uid, positionUid, figi, ticker, isin, lot, currency, nominalInCurrency, price, lastPrice, nominal, minPriceIncrement, minPriceIncrementAmount);
    }
}
