package ru.webinnovations.template_web_project.db.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class MarginAttributeModel {
    private MoneyValueModel liquidPortfolio;
    private MoneyValueModel startingMargin;
    private MoneyValueModel minimalMargin;
    private QuotationValue fundSufficiencyLevel;
    private MoneyValueModel missingFundsAmount;
    private MoneyValueModel correctedMargin;
}
