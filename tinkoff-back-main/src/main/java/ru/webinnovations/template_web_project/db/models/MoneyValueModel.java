package ru.webinnovations.template_web_project.db.models;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class MoneyValueModel {
    private BigDecimal value;
    private String currency;
}
