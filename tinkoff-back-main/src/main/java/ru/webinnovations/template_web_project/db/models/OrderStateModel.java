package ru.webinnovations.template_web_project.db.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@Builder
@Slf4j
public class OrderStateModel {
    private String orderId;
    private String orderExecutionReportStatus;
    private long lotsRequested;
    private long lotsExecuted;
    private MoneyValueModel initialOrderPrice;
    private MoneyValueModel executedOrderPrice;
    private MoneyValueModel totalOrderAmount;
    private MoneyValueModel initialCommission;
    private MoneyValueModel executedCommission;

    public boolean allExecuted() {
        return this.lotsExecuted == this.lotsRequested && this.lotsRequested != 0L;
    }
}
