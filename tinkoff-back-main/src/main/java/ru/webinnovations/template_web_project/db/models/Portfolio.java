package ru.webinnovations.template_web_project.db.models;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class Portfolio {
    //общая стоимость облигаций в портфеле
    private MoneyValueModel totalAmountBonds;
    //общая стоимость фондов в портфеле
    private MoneyValueModel totalAmountEtf;
    //общая стоимость валют в портфеле
    private MoneyValueModel totalAmountCurrencies;
    //общая стоимость фьючерсов в портфеле
    private MoneyValueModel totalAmountFutures;
    //общая стоимость акций в портфеле
    private MoneyValueModel totalAmountShares;
    //текущая доходность портфеля
    private BigDecimal expectedYield;
}
