package ru.webinnovations.template_web_project.db.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
public class QuotationValue {
    private BigDecimal value;
}
