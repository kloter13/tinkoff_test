package ru.webinnovations.template_web_project.db.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.webinnovations.template_web_project.db.enums.OrderTypeEnum;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "strategy")
@Getter
@Setter
@ToString
public class Strategy {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StrategyExecutionStatus status;

    @Column(name = "volume")
    private Long volume;

    @Column(name = "buy_price")
    private String buyPrice;

    @Column(name = "sell_price")
    private String sellPrice;

    @Column(name = "date_of_begin")
    private LocalDateTime dateOfBegin;

    @Column(name = "date_of_end")
    private LocalDateTime dateOfEnd;
    @Column(name = "pause_sell")
    private boolean pauseSell;
    @Column(name = "pause_buy")
    private boolean pauseBuy;

    @Column(name = "price_changed")
    private boolean isPriceChanged;

    @Enumerated(EnumType.STRING)
    @Column(name = "order_type")
    private OrderTypeEnum orderType;


    @ManyToOne(fetch = FetchType.EAGER)
    private Instrument instrument;

    @OneToMany(mappedBy = "strategy", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<StrategyExecutionHistory> history;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Transient
    private int executedOperation;
}
