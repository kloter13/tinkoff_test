package ru.webinnovations.template_web_project.db.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionType;
import ru.webinnovations.template_web_project.db.enums.StrategyOrderStatus;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "strategy_execution_history")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StrategyExecutionHistory {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_id")
    private String orderId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "strategy_id", nullable = false)
    @JsonIgnore
    private Strategy strategy;

    @Column(name = "execution_timestamp")
    private LocalDateTime executionTimestamp;

    @Column(name = "volume")
    private Long volume;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private StrategyExecutionType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StrategyOrderStatus status;

}
