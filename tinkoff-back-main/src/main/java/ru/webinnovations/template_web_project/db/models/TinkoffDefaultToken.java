package ru.webinnovations.template_web_project.db.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tinkoff_default_token")
@Data
public class TinkoffDefaultToken {

    @Id
    @Column
    private Integer id;
    @Column(nullable = false)
    private String token;
}
