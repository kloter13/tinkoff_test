package ru.webinnovations.template_web_project.db.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;
import ru.webinnovations.template_web_project.db.models.Instrument;

import java.util.List;

@Repository
public interface InstrumentRepository extends JpaRepository<Instrument, Long> {

    @Query("SELECT i FROM Instrument i " +
            "WHERE (:instrumentType is null or i.type = :instrumentType) " +
            "AND (:name is null or lower(i.name) like lower(concat('%', :name, '%'))) " +
            "AND (:ticker is null or lower(i.ticker) like lower(concat('%', :ticker, '%')))")
    Page<Instrument> findByParameters(InstrumentTypeEnum instrumentType, String name,
                                      String ticker, Pageable pageable);

    boolean existsByFigi(String figi);

    List<Instrument> findAllByType(InstrumentTypeEnum type);

}
