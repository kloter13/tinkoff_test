package ru.webinnovations.template_web_project.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;

import java.util.Optional;

@Repository
public interface StrategyExecutionHistoryRepository extends JpaRepository<StrategyExecutionHistory, Long> {
    Optional<StrategyExecutionHistory> findByStrategy(Strategy strategy);
    Optional<StrategyExecutionHistory> findByOrderId(String orderId);
}
