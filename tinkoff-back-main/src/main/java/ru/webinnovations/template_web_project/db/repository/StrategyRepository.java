package ru.webinnovations.template_web_project.db.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.dto.StrategyDTO;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface StrategyRepository extends JpaRepository<Strategy, Long> {

    List<Strategy> getByUser(User user, Sort sort);
}
