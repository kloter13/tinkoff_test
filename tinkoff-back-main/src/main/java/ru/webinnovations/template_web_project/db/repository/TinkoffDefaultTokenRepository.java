package ru.webinnovations.template_web_project.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.webinnovations.template_web_project.db.models.TinkoffDefaultToken;

@Repository
public interface TinkoffDefaultTokenRepository extends JpaRepository<TinkoffDefaultToken, Integer> {
}
