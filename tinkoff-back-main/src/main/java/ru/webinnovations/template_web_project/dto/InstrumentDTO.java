package ru.webinnovations.template_web_project.dto;

import lombok.Data;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;

import java.util.List;
import java.util.Objects;

@Data
public class InstrumentDTO {

    private Long id;
    private String name;
    private InstrumentTypeEnum type;
    private String uid;
    private String positionUid;
    private String figi;
    private String ticker;
    private String isin;
    private String lot;
    private String currency;
    private String nominalInCurrency;
    private String price;
    private String lastPrice;
    private Boolean isAvailable;
    private String nominal;
    private String minPriceIncrement;
    private String minPriceIncrementAmount;
    private List<LastPriceDto> lastPriceList;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InstrumentDTO that = (InstrumentDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && type == that.type && Objects.equals(uid, that.uid) && Objects.equals(positionUid, that.positionUid) && Objects.equals(figi, that.figi) && Objects.equals(ticker, that.ticker) && Objects.equals(isin, that.isin) && Objects.equals(lot, that.lot) && Objects.equals(currency, that.currency) && Objects.equals(nominalInCurrency, that.nominalInCurrency) && Objects.equals(price, that.price) && Objects.equals(lastPrice, that.lastPrice) && Objects.equals(nominal, that.nominal) && Objects.equals(minPriceIncrement, that.minPriceIncrement) && Objects.equals(minPriceIncrementAmount, that.minPriceIncrementAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, uid, positionUid, figi, ticker, isin, lot, currency, nominalInCurrency, price, lastPrice, nominal, minPriceIncrement, minPriceIncrementAmount);
    }
}
