package ru.webinnovations.template_web_project.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class LastPriceDto {

    private Long openUnits;
    private Integer openNano;
    private Long closeUnits;
    private Integer closeNano;
    private Long highUnits;
    private Integer highNano;
    private Long lowUnits;
    private Integer lowNano;
    private LocalDateTime timestamp;
    private Boolean isPriceComplete;
}
