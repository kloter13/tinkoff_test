package ru.webinnovations.template_web_project.dto;

import lombok.Data;
import ru.webinnovations.template_web_project.db.models.Role;

@Data
public class RoleDto {

   private Long id;
   private String name;

   public static RoleDto toDto(Role role) {
      RoleDto result = new RoleDto();

      result.setId(role.getId());
      result.setName(role.getName());

      return result;
   }

}
