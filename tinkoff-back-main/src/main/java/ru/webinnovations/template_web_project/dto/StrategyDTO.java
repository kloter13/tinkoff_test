package ru.webinnovations.template_web_project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import ru.webinnovations.template_web_project.db.enums.OrderTypeEnum;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class StrategyDTO {

    private Long id;
    private Long volume;
    private String buyPrice;
    private String sellPrice;
    private LocalDateTime dateOfBegin;
    private LocalDateTime dateOfEnd;
    private boolean pauseSell;
    private boolean pauseBuy;
    private boolean priceChanged;
    private OrderTypeEnum orderType;
    private InstrumentDTO instrumentDTO;
    private List<StrategyExecutionHistoryDTO> history;
    private StrategyExecutionStatus status;
    private int executedOperation;
}

