package ru.webinnovations.template_web_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionStatus;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionType;
import ru.webinnovations.template_web_project.db.models.Strategy;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StrategyExecutionHistoryDTO {

    @NotNull
    private Long id;
    private String orderId;
    private Long strategyId;
    private LocalDateTime executionTimestamp;
    private Long value;
    private StrategyExecutionType type;
    private StrategyExecutionStatus status;
}
