package ru.webinnovations.template_web_project.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import ru.webinnovations.template_web_project.db.models.User;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto {

    private Long id;
    private String username;
    private String name;
    private Boolean isTokenCreated;
    private RoleDto role;
    private List<StrategyDTO> strategies;

}
