package ru.webinnovations.template_web_project.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@Builder
public class LoginPasswordPut {
    @NotBlank
    private String currentLogin;
    @NotBlank
    private String currentPassword;
    private String newLogin;
    private String newPassword;
}
