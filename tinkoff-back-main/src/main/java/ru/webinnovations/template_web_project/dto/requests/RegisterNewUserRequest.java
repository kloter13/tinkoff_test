package ru.webinnovations.template_web_project.dto.requests;

import lombok.Data;

@Data
public class RegisterNewUserRequest {

    private String username;
    private String password;
    private String name;
}
