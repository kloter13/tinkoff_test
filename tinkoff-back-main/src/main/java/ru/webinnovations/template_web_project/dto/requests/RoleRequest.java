package ru.webinnovations.template_web_project.dto.requests;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RoleRequest {
    @NotBlank
    private String name;
}
