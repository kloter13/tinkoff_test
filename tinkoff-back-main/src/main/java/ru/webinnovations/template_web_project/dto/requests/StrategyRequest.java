package ru.webinnovations.template_web_project.dto.requests;

import lombok.Data;
import ru.webinnovations.template_web_project.dto.StrategyDTO;

import javax.validation.constraints.NotNull;

@Data
public class StrategyRequest {
    @NotNull
    private StrategyDTO strategyDTO;

}
