package ru.webinnovations.template_web_project.dto.requests;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TinkoffTokenRequest {
    @NotBlank
    private String token;
}
