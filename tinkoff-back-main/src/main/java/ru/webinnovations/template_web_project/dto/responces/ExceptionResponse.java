package ru.webinnovations.template_web_project.dto.responces;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ExceptionResponse {
    private String message;
}
