package ru.webinnovations.template_web_project.dto.responces;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;

@Data
@AllArgsConstructor
@Builder
public class InstrumentShortResponse {
    private String figi;
    private String uid;
    private InstrumentTypeEnum type;
    private String ticker;
    private String status;
}
