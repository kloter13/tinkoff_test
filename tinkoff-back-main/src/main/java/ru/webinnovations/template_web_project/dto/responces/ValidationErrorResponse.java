package ru.webinnovations.template_web_project.dto.responces;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ValidationErrorResponse extends ExceptionResponse {
    private String message;
    private final Map<String, String> errors;

    public ValidationErrorResponse(String message, Map<String, String> errors) {
        super(message);
        this.errors = errors;
    }
}
