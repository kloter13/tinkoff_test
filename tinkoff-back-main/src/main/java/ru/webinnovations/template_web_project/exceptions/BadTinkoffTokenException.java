package ru.webinnovations.template_web_project.exceptions;

public class BadTinkoffTokenException extends RuntimeException {
    public BadTinkoffTokenException(String message) {
        super(message);
    }
}
