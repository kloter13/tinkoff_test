package ru.webinnovations.template_web_project.exceptions;

public class InstrumentNotAvailableException extends RuntimeException{
    public InstrumentNotAvailableException(String message){
        super(message);
    }
}
