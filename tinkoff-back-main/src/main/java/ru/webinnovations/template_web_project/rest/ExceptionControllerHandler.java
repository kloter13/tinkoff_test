package ru.webinnovations.template_web_project.rest;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.tinkoff.piapi.core.exception.ApiRuntimeException;
import ru.webinnovations.template_web_project.dto.responces.ExceptionResponse;
import ru.webinnovations.template_web_project.dto.responces.ValidationErrorResponse;
import ru.webinnovations.template_web_project.exceptions.InstrumentNotAvailableException;
import ru.webinnovations.template_web_project.exceptions.JwtAuthenticationException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestControllerAdvice
public class ExceptionControllerHandler {
    private Map<String, String> prepareValidationErrors(List<FieldError> errors) {
        Map<String, String> result = new HashMap<>();
        for (FieldError error : errors) {
            result.put(error.getField(), "Field has wrong value " + error.getRejectedValue() + " : " + error.getDefaultMessage());
        }
        return result;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public ExceptionResponse handleExceptionFromDatabase(RuntimeException exception) {
        return new ExceptionResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InstrumentNotAvailableException.class)
    public ExceptionResponse handleInstrumentNotAvailableException(InstrumentNotAvailableException exception) {
        return new ExceptionResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ApiRuntimeException.class)
    public ExceptionResponse handleTinkoffApiException(ApiRuntimeException exception) {
        return new ExceptionResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InterruptedException.class)
    public ExceptionResponse handleInterruptedException(InterruptedException exception) {
        return new ExceptionResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ExecutionException.class)
    public ExceptionResponse handleExecutionException(ExecutionException exception) {
        return new ExceptionResponse(exception.getMessage());
    }


    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(EntityExistsException.class)
    public ExceptionResponse existEntityException(EntityExistsException exception) {
        return new ExceptionResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public ExceptionResponse notFoundException(EntityNotFoundException exception) {
        return new ExceptionResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ValidationErrorResponse badRequest(MethodArgumentNotValidException exception) {
        var validationErrors = prepareValidationErrors(exception.getBindingResult().getFieldErrors());
        return new ValidationErrorResponse("Validation failed", validationErrors);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({JwtException.class,
            IllegalArgumentException.class,
            JwtAuthenticationException.class,
            MalformedJwtException.class})
    public ExceptionResponse badJwtToken(Exception exception) {
        return new ExceptionResponse(exception.getMessage());
    }

}
