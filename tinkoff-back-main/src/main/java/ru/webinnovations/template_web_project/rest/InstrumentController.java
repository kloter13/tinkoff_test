package ru.webinnovations.template_web_project.rest;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.piapi.contract.v1.CandleInterval;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;
import ru.webinnovations.template_web_project.db.models.Portfolio;
import ru.webinnovations.template_web_project.dto.InstrumentDTO;
import ru.webinnovations.template_web_project.dto.responces.InstrumentShortResponse;
import ru.webinnovations.template_web_project.services.InstrumentService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/api/instrument")
public class InstrumentController {

    private InstrumentService instrumentService;


    @Autowired
    public InstrumentController(InstrumentService instrumentService) {
        this.instrumentService = instrumentService;
    }

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Page<InstrumentDTO>> searchInstruments(
            @RequestParam(required = false) InstrumentTypeEnum instrumentType,
            @RequestParam(required = false, defaultValue = "") String name,
            @RequestParam(required = false, defaultValue = "") String ticker,
            @RequestParam(required = false, defaultValue = "0") int offset,
            @RequestParam(required = false, defaultValue = "0") int pageNumber,
            @RequestParam(required = false, defaultValue = "10") int pageSize,
            @RequestParam(required = false, defaultValue = "false") boolean sorted,
            @RequestParam(required = false, defaultValue = "false") boolean unpaged) {
        return ResponseEntity.ok(instrumentService.searchByParameters(instrumentType, name, ticker, offset, pageNumber, pageSize, sorted, unpaged));
    }

    @GetMapping("/fromTinkoffApi")
    @PreAuthorize("hasRole('USER')")
    public List<InstrumentShortResponse> getInstrumentsFromTinkoffApi(@RequestParam InstrumentTypeEnum type) {
        return instrumentService.getInstrumentsFromTinkoffApi(type);
    }

    @GetMapping("/withCandles")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<InstrumentDTO> getInstrumentWithCandles(@RequestParam(required = false) Long instrumentId,
                                                                  @RequestParam(required = false) LocalDateTime from,
                                                                  @RequestParam(required = false) LocalDateTime to,
                                                                  @RequestParam(required = false) CandleInterval candleInterval
    ) {

        return ResponseEntity.ok(instrumentService.getInstrumentWithCandles(instrumentId, from, to, candleInterval));
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<List<InstrumentDTO>> getAll() {
        return ResponseEntity.ok(instrumentService.getAll());
    }

    @GetMapping("/portfolio")
    @PreAuthorize("hasRole('USER')")
    public Portfolio getPortfolio() {
        return instrumentService.getPortfolio();
    }

    @Hidden
    @DeleteMapping("/{type}")
    public void deleteInstrumentByType(@PathVariable("type") InstrumentTypeEnum type){
        instrumentService.deleteByType(type);
    }

}
