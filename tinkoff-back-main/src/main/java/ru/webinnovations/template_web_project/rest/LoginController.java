package ru.webinnovations.template_web_project.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.dto.UserDto;
import ru.webinnovations.template_web_project.dto.requests.LoginPasswordPut;
import ru.webinnovations.template_web_project.dto.requests.LoginRequest;
import ru.webinnovations.template_web_project.dto.requests.RegisterNewUserRequest;
import ru.webinnovations.template_web_project.dto.responces.LoginResponseDto;
import ru.webinnovations.template_web_project.security.jwt.JwtTokenProvider;
import ru.webinnovations.template_web_project.services.UserService;
import ru.webinnovations.template_web_project.services.mapper.UserMapper;


@RestController
@RequestMapping(value = "/api/login/")
public class LoginController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public LoginController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider,
                           UserService userService, UserMapper userMapper) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping("auth")
    public ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(), loginRequest.getPassword()
                )
        );
        if (authentication.isAuthenticated()) {
            String username = loginRequest.getUsername();
            User user = userService.findByUsername(username);
            String token = jwtTokenProvider.generateToken(loginRequest.getUsername());
            return ResponseEntity.ok(new LoginResponseDto(username, token, user.getIsTokenCreated() != null));
        } else {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @PostMapping("register")
    public ResponseEntity<UserDto> register(@RequestBody RegisterNewUserRequest registerNewUserRequest) {
        User user = new User();
        user.setUsername(registerNewUserRequest.getUsername());
        user.setPassword(registerNewUserRequest.getPassword());
        user.setName(registerNewUserRequest.getName());

        return ResponseEntity.ok(userMapper.toDto(userService.register(user)));
    }

    @PutMapping("update")
    public ResponseEntity<LoginResponseDto> updateLoginPassword(@RequestBody LoginPasswordPut request) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getCurrentLogin(), request.getCurrentPassword()
                )
        );
        if (authentication.isAuthenticated()) {
            UserDto userDto = userService.updateLoginPassword(request);
            String token = jwtTokenProvider.generateToken(request.getNewLogin());
            return ResponseEntity.ok(new LoginResponseDto(request.getNewLogin(), token, userDto.getIsTokenCreated() != null));
        } else {
            throw new BadCredentialsException("Invalid username or password");
        }
    }
}
