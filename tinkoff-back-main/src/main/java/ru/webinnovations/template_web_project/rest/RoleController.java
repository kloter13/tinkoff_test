package ru.webinnovations.template_web_project.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.webinnovations.template_web_project.db.models.Role;
import ru.webinnovations.template_web_project.dto.requests.RoleRequest;
import ru.webinnovations.template_web_project.dto.responces.RoleResponse;
import ru.webinnovations.template_web_project.services.impl.RoleServiceImpl;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/api/roles/")
public class RoleController {
    private final RoleServiceImpl roleService;

    @Autowired
    public RoleController(RoleServiceImpl roleService) {
        this.roleService = roleService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<RoleResponse> getRoles() {
        return roleService.getRoles();
    }

    @GetMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public RoleResponse getRole(@PathVariable("id") Long id) {
        return roleService.getRole(id);
    }

    @GetMapping("byName/{name}")
    @PreAuthorize("hasRole('ADMIN')")
    public RoleResponse getRole(@PathVariable("name") String name) {
        return roleService.getRole(name);
    }

    @PutMapping("/{roleId}")
    @PreAuthorize("hasRole('ADMIN')")
    public RoleResponse updateRole(@PathVariable("roleId") Long roleId, @RequestBody @Valid RoleRequest rolePut) {
        return roleService.updateRole(roleId, rolePut);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public RoleResponse createRole(@RequestBody RoleRequest roleRequest) {
        return roleService.createRole(roleRequest);
    }
}
