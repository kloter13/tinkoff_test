package ru.webinnovations.template_web_project.rest;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.webinnovations.template_web_project.configs.ContextProvider;
import ru.webinnovations.template_web_project.db.models.MarginAttributeModel;
import ru.webinnovations.template_web_project.db.models.OrderStateModel;
import ru.webinnovations.template_web_project.dto.StrategyDTO;
import ru.webinnovations.template_web_project.dto.requests.StrategyRequest;
import ru.webinnovations.template_web_project.services.StrategyService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/strategy")
@Slf4j
public class StrategyController {

    private final ContextProvider contextProvider;
    private final StrategyService strategyService;

    @Autowired
    public StrategyController(ContextProvider contextProvider, StrategyService strategyService) {
        this.contextProvider = contextProvider;
        this.strategyService = strategyService;
    }


    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<StrategyDTO> createStrategy(@RequestBody StrategyDTO strategyDTO) {
        return ResponseEntity.ok(strategyService.create(strategyDTO));
    }

    @Hidden
    @GetMapping("/test/{uid}")
    public void isAvailableInstrument(@PathVariable("uid") String uid) {
        strategyService.getSchedule(uid);
    }

    @Hidden
    @GetMapping("/test-sell")
    public void testSell() {
        strategyService.checkAndExecuteStrategy();
    }

    @Hidden
    @GetMapping("/test-comparison")
    public void testComparison(){
        strategyService.testDoubleComparison();
    }

    @GetMapping("/state/{orderId}")
    @PreAuthorize("hasRole('USER')")
    public OrderStateModel orderState(@PathVariable("orderId") String orderId) {
        return strategyService.getOrderState(orderId);
    }


    @GetMapping("/margins")
    @PreAuthorize("hasRole('USER')")
    public MarginAttributeModel getMargins() {
        return strategyService.getMarginAttributes();
    }


    @PutMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<StrategyDTO> updateStrategy(@RequestBody StrategyDTO strategyDTO, @PathVariable("id") Long id) {
        return ResponseEntity.ok(strategyService.update(strategyDTO, id));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<StrategyDTO> getStrategyById(@PathVariable Long id) {
        return ResponseEntity.ok(strategyService.getById(id));
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/byUser")
    public ResponseEntity<List<StrategyDTO>> getStrategyByUser() {
        return ResponseEntity.ok(strategyService.getByUser());
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<StrategyDTO>> getAllStrategies() {
        return ResponseEntity.ok(strategyService.getAll());
    }

    @DeleteMapping
    @PreAuthorize("hasRole('USER')")
    public void deleteStrategyById(@RequestParam Long id) {
        strategyService.deleteById(id);
    }
}
