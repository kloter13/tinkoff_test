package ru.webinnovations.template_web_project.rest;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;
import ru.webinnovations.template_web_project.dto.StrategyExecutionHistoryDTO;
import ru.webinnovations.template_web_project.services.impl.StrategyExecutionHistoryServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/api/strategy-execution-histories/")
public class StrategyExecutionHistoryController {
    private final StrategyExecutionHistoryServiceImpl strategyExecutionHistoryService;

    @Autowired
    public StrategyExecutionHistoryController(StrategyExecutionHistoryServiceImpl strategyExecutionHistoryService) {
        this.strategyExecutionHistoryService = strategyExecutionHistoryService;
    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    StrategyExecutionHistoryDTO create(@RequestBody StrategyExecutionHistoryDTO strategyExecutionHistoryDTO) {
        return strategyExecutionHistoryService.create(strategyExecutionHistoryDTO);
    }

    @GetMapping("{strategyId}")
    @PreAuthorize("hasRole('USER')")
    List<StrategyExecutionHistoryDTO> gteByStrategy(@PathVariable("strategyId") Long strategyId) {
        return strategyExecutionHistoryService.getStrategyExecutionHistoryByStrategy(strategyId);
    }

    @PutMapping
    @PreAuthorize("hasRole('USER')")
    StrategyExecutionHistoryDTO update(@RequestBody StrategyExecutionHistoryDTO strategyExecutionHistoryDTO) {
        return strategyExecutionHistoryService.update(strategyExecutionHistoryDTO);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('USER')")
    public void delete(@PathVariable("id") Long id) {
        strategyExecutionHistoryService.delete(id);
    }

    @Hidden
    @GetMapping("/by-order-id/{orderId}")
    public StrategyExecutionHistory getHistoryById(@PathVariable("orderId") String orderId){
        return strategyExecutionHistoryService.getByOrderId(orderId);
    }

}
