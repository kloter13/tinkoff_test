package ru.webinnovations.template_web_project.rest;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.piapi.core.InvestApi;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.dto.StrategyDTO;
import ru.webinnovations.template_web_project.services.TestService;
import ru.webinnovations.template_web_project.services.mapper.StrategyMapper;

@RestController
@RequestMapping("/api/test")
@Hidden
public class TestController {
    private final TestService testService;
    private InvestApi sandboxApi;
    private final StrategyMapper strategyMapper;

    @Autowired
    public TestController(TestService testService, StrategyMapper strategyMapper) {
        this.testService = testService;
        this.strategyMapper = strategyMapper;
    }

    @GetMapping("/open/{token}")
    public void openAccount(@PathVariable("token") String token){
        sandboxApi = testService.getAccount(token);
    }

    @GetMapping("/portfolio")
    public void getPortfolio() {
        testService.getPortfolioExample(sandboxApi);
    }

    @PostMapping("/post")
    public Strategy postOrder(@RequestBody StrategyDTO strategyDTO) {
       return testService.ordersServiceExample(sandboxApi, strategyMapper.toEntity(strategyDTO));
    }
}
