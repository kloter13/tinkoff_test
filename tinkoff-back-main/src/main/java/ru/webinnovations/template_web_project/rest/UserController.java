package ru.webinnovations.template_web_project.rest;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.dto.UserDto;
import ru.webinnovations.template_web_project.dto.requests.TinkoffTokenRequest;
import ru.webinnovations.template_web_project.services.UserService;
import ru.webinnovations.template_web_project.services.mapper.UserMapper;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/users/")
@Slf4j
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @Hidden
    @GetMapping("/all")
    public List<UserDto> getAll() {
        return userMapper.toDto(userService.getAll());
    }

    @Hidden
    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable("userId") Long userId){
        userService.delete(userId);
    }

    @GetMapping("me")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ResponseEntity<UserDto> me() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto userDto = userMapper.toDto(userService.findByUsername(authentication.getName()));

        return ResponseEntity.ok(userDto);
    }

    @GetMapping(value = "{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDto> getUserById(@PathVariable(name = "id") Long id) {
        User user = userService.findById(id);
        UserDto result = userMapper.toDto(user);

        return ResponseEntity.ok(result);
    }

    @PostMapping("token")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ResponseEntity<UserDto> setUserTinkoffToken(@RequestBody @Valid TinkoffTokenRequest tinkoffTokenRequest) {
        UserDto userDto = userService.setTinkoffToken(tinkoffTokenRequest.getToken());

        return ResponseEntity.ok(userDto);
    }

    @PutMapping("token")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ResponseEntity<UserDto> updateUserTinkoffToken(@RequestBody @Valid TinkoffTokenRequest request) {
        UserDto userDto = userService.updateTinkoffToken(request.getToken());

        return ResponseEntity.ok(userDto);
    }
}
