package ru.webinnovations.template_web_project.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.webinnovations.template_web_project.services.InstrumentService;
import ru.webinnovations.template_web_project.services.StrategyService;

@Slf4j
@Component
public class ScheduledHandler {

    private final StrategyService strategyService;
    private final InstrumentService instrumentService;

    @Autowired
    public ScheduledHandler(StrategyService strategyService, InstrumentService instrumentService) {
        this.strategyService = strategyService;
        this.instrumentService = instrumentService;
    }
    @Scheduled(cron = "${scheduled.taskCheckStrategies.cron}")
    public void performTaskCheckStrategies() {
        strategyService.checkAndExecuteStrategy();
    }

    @Scheduled(cron = "${scheduled.taskUpdateInstruments.cron}")
    public void performTaskUpdateInstruments() {
        instrumentService.create();
    }

    @Scheduled(cron = "0 0/7 * * * ?")
    public void relaunchPausedStrategies(){strategyService.launchPausedStrategy();}

    @Scheduled(cron = "0 0/35 * * * ?")
    public void unpausesStrategies() {strategyService.unpauseStrategies();}

}
