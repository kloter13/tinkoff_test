package ru.webinnovations.template_web_project.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.tinkoff.piapi.contract.v1.CandleInterval;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;
import ru.webinnovations.template_web_project.db.models.Portfolio;
import ru.webinnovations.template_web_project.dto.InstrumentDTO;
import ru.webinnovations.template_web_project.dto.LastPriceDto;
import ru.webinnovations.template_web_project.dto.responces.InstrumentShortResponse;

import java.time.LocalDateTime;
import java.util.List;

public interface InstrumentService {

    void create();

    public Page<InstrumentDTO> searchByParameters(InstrumentTypeEnum instrumentType, String name,
                                               String ticker, Pageable pageable);
    List<InstrumentDTO> getAll();

    List<LastPriceDto> getLastPricesByInstrumentId(String instrumentId, LocalDateTime from,
                                                   LocalDateTime to, CandleInterval candleInterval);

    InstrumentDTO getInstrumentWithCandles(Long instrumentId, LocalDateTime from,
                                           LocalDateTime to, CandleInterval candleInterval);

    InstrumentDTO getById(Long id);

    Page<InstrumentDTO> searchByParameters(InstrumentTypeEnum instrumentType, String name, String ticker, int offset, int pageNumber, int pageSize, boolean sorted, boolean unpaged);

    Portfolio getPortfolio();

    int getQuantityExecutedOperations(String figi, LocalDateTime from, LocalDateTime to);

    boolean existsByFigi(String figi);

    List<InstrumentShortResponse> getInstrumentsFromTinkoffApi(InstrumentTypeEnum type);

    void deleteByType(InstrumentTypeEnum type);
}
