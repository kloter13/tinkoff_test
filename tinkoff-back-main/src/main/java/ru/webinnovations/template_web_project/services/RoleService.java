package ru.webinnovations.template_web_project.services;

import ru.webinnovations.template_web_project.dto.requests.RoleRequest;
import ru.webinnovations.template_web_project.dto.responces.RoleResponse;

import java.util.List;

public interface RoleService {
    RoleResponse getRole(Long id);

    RoleResponse getRole(String name);

    List<RoleResponse> getRoles();

    RoleResponse createRole(RoleRequest roleRequest);

    RoleResponse updateRole(Long id, RoleRequest roleRequest);
}
