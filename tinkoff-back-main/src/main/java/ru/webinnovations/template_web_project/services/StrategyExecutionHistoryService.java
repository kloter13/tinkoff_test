package ru.webinnovations.template_web_project.services;

import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;
import ru.webinnovations.template_web_project.dto.StrategyExecutionHistoryDTO;

import java.util.List;
import java.util.Optional;

public interface StrategyExecutionHistoryService {
    StrategyExecutionHistoryDTO create(StrategyExecutionHistoryDTO request);

    StrategyExecutionHistoryDTO update(StrategyExecutionHistoryDTO strategyExecutionHistoryDTO);

    List<StrategyExecutionHistoryDTO> getStrategyExecutionHistoryByStrategy(Long strategyId);

    void delete(Long id);
    StrategyExecutionHistory getByOrderId(String orderId);

    StrategyExecutionHistory update(StrategyExecutionHistory strategyExecutionHistory, Long id);
}
