package ru.webinnovations.template_web_project.services;

import ru.webinnovations.template_web_project.db.models.MarginAttributeModel;
import ru.webinnovations.template_web_project.db.models.OrderStateModel;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.dto.StrategyDTO;

import java.util.List;

public interface StrategyService {


    StrategyDTO create(StrategyDTO strategyDTO);

    StrategyDTO update(StrategyDTO strategyDTO, Long id);

    StrategyDTO getById(Long id);

    List<StrategyDTO> getAll();

    List<StrategyDTO> getByUser();

    void checkAndExecuteStrategy();

    void deleteById(Long id);

    boolean isAvailableInstrument(String instrumentUid);

    void getSchedule(String instrumentFigi);

    OrderStateModel getOrderState(String orderId);

    MarginAttributeModel getMarginAttributes();

    long getTotalExecutedLots(Strategy strategy);

    void launchPausedStrategy();

    void testDoubleComparison();

    void unpauseStrategies();
}
