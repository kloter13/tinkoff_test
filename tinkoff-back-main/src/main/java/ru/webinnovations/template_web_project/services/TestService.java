package ru.webinnovations.template_web_project.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.contract.v1.*;
import ru.tinkoff.piapi.core.InvestApi;
import ru.tinkoff.piapi.core.MarketDataService;
import ru.tinkoff.piapi.core.OrdersService;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionStatus;
import ru.webinnovations.template_web_project.db.enums.StrategyExecutionType;
import ru.webinnovations.template_web_project.db.enums.StrategyOrderStatus;
import ru.webinnovations.template_web_project.db.models.MoneyValueModel;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class TestService {
    private final UserService userService;

    @Autowired
    public TestService(UserService userService) {
        this.userService = userService;
    }

    public InvestApi getAccount(String token) {
        InvestApi sandboxApi =
                InvestApi.createSandbox(token);
        var accounts = sandboxApi.getUserService().getAccountsSync();
        var mainAccount = accounts.get(0);

        /*sandboxApi.getSandboxService().payIn(mainAccount.getId(), MoneyValueModel.newBuilder().setUnits(10000).setCurrency("RUB").build());
        sandboxApi.getSandboxService().payIn(mainAccount.getId(), MoneyValueModel.newBuilder().setUnits(10000).setCurrency("USD").build());*/
        return sandboxApi;
    }

    public int getQuantityExecutedOperations(InvestApi api, String figi, LocalDateTime from, LocalDateTime to) {
        var accountList = api.getUserService().getAccountsSync();
        var mainAccount = accountList.get(0).getId();
        var executedOperations = api.getOperationsService().getExecutedOperationsSync(
                mainAccount, from.atZone(ZoneId.of("Africa/Cairo")).toInstant(),
                to.atZone(ZoneId.of("Africa/Cairo")).toInstant(), figi
        );

        return executedOperations.size();
    }

    public Strategy ordersServiceExample(InvestApi api, Strategy strategy) {
        //Выставляем заявку
        var accounts = api.getUserService().getAccountsSync();
        var mainAccount = accounts.get(0).getId();

        System.out.println("In with " + strategy);
        OrdersService ordersService = api.getOrdersService();
        MarketDataService marketDataService = api.getMarketDataService();

        try {
            LastPrice lastPrice = marketDataService
                    .getLastPrices(Collections.singleton(
                            strategy.getInstrument().getUid()
                    ))
                    .get()
                    .stream()
                    .findFirst()
                    .orElse(null);
            if (lastPrice == null) {
                throw new EntityNotFoundException("Null last price");
            }
            log.info("market last price {} , strategy last price {}", lastPrice.getPrice(), Long.parseLong(strategy.getBuyPrice()));
            if (Long.parseLong(strategy.getBuyPrice()) >= lastPrice.getPrice().getUnits()) {
                PostOrderResponse response =
                        ordersService.postOrder(
                                strategy.getInstrument().getFigi(),
                                strategy.getVolume(),
                                lastPrice.getPrice(),
                                OrderDirection.ORDER_DIRECTION_BUY,
                                mainAccount,
                                OrderType.ORDER_TYPE_MARKET,
                                UUID.randomUUID().toString()).get();

                List<StrategyExecutionHistory> histories =
                        strategy.getHistory() == null ? new ArrayList<>() : strategy.getHistory();
                histories.add(StrategyExecutionHistory.builder()
                        .type(StrategyExecutionType.BUY)
                        .executionTimestamp(LocalDateTime.now())
                        .volume(response.getLotsExecuted())
                        .strategy(strategy)
                        .status(StrategyOrderStatus.valueOf(response.getExecutionReportStatus().toString()))
                        .orderId(response.getOrderId())
                        .build()
                );
                strategy.setHistory(histories);
                strategy.setStatus(StrategyExecutionStatus.SUCCESS);
                strategy.setExecutedOperation(
                        getQuantityExecutedOperations(api,
                                strategy.getInstrument().getFigi(), strategy.getDateOfBegin(), strategy.getDateOfEnd()
                        )
                );

            }

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        //Получаем список активных заявок, проверяем наличие нашей заявки в списке
        var orders = api.getOrdersService().getOrdersSync(mainAccount);
        if (orders.stream().anyMatch(el -> strategy.getHistory().get(0).getOrderId().equals(el.getOrderId()))) {
            log.info("заявка с id {} есть в списке активных заявок", strategy.getHistory().get(0).getOrderId());
        }
        return strategy;

    }


    public void getPortfolioExample(InvestApi api) {
        var accounts = api.getUserService().getAccountsSync();
        var mainAccount = accounts.get(0).getId();

        //Получаем и печатаем портфолио
        var portfolio = api.getOperationsService().getPortfolioSync(mainAccount);
        var totalAmountBonds = portfolio.getTotalAmountBonds();
        log.info("общая стоимость облигаций в портфеле {} currency {}", totalAmountBonds.getValue(), totalAmountBonds.getCurrency());

        var totalAmountEtf = portfolio.getTotalAmountEtfs();
        log.info("общая стоимость фондов в портфеле {} currency {}", totalAmountEtf.getValue(), totalAmountEtf.getCurrency());

        var totalAmountCurrencies = portfolio.getTotalAmountCurrencies();
        log.info("общая стоимость валют в портфеле {} currency {}", totalAmountCurrencies.getValue(), totalAmountCurrencies.getCurrency());

        var totalAmountFutures = portfolio.getTotalAmountFutures();
        log.info("общая стоимость фьючерсов в портфеле {} currency {}", totalAmountFutures.getValue(), totalAmountFutures.getCurrency());

        var totalAmountShares = portfolio.getTotalAmountShares();
        log.info("общая стоимость акций в портфеле {} currency {}", totalAmountShares.getValue(), totalAmountShares.getCurrency());

        log.info("текущая доходность портфеля {}", portfolio.getExpectedYield());

        var positions = portfolio.getPositions();
        log.info("в портфолио {} позиций", positions.size());
        for (int i = 0; i < Math.min(positions.size(), 5); i++) {
            var position = positions.get(i);
            var figi = position.getFigi();
            var quantity = position.getQuantity();
            var currentPrice = position.getCurrentPrice();
            var expectedYield = position.getExpectedYield();
            log.info(
                    "позиция с figi: {}, количество инструмента: {}, текущая цена инструмента: {}, текущая расчитанная " +
                            "доходность: {}",
                    figi, quantity, currentPrice, expectedYield);
        }

    }
}
