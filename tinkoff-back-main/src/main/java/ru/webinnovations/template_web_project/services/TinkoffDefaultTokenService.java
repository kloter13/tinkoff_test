package ru.webinnovations.template_web_project.services;

import ru.webinnovations.template_web_project.db.models.TinkoffDefaultToken;

import java.util.List;

public interface TinkoffDefaultTokenService {
    List<TinkoffDefaultToken> getDefaultsToken();
    void deleteDefaultToken(Integer id);
    TinkoffDefaultToken updateDefaultToken(String token);
    TinkoffDefaultToken addDefaultToken(String token);
}
