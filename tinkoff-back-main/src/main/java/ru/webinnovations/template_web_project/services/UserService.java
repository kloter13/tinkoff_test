package ru.webinnovations.template_web_project.services;

import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.dto.UserDto;
import ru.webinnovations.template_web_project.dto.requests.LoginPasswordPut;

import java.util.List;

public interface UserService {

    User register(User user);

    List<User> getAll();

    User findByUsername(String username);

    User findById(Long id);

    UserDto setTinkoffToken(String token);

    UserDto updateTinkoffToken(String newToken);

    void delete(Long id);

    UserDto updateLoginPassword(LoginPasswordPut request);
}
