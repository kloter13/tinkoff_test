package ru.webinnovations.template_web_project.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.piapi.contract.v1.*;
import ru.tinkoff.piapi.core.InstrumentsService;
import ru.tinkoff.piapi.core.InvestApi;
import ru.tinkoff.piapi.core.MarketDataService;
import ru.webinnovations.template_web_project.configs.ContextProvider;
import ru.webinnovations.template_web_project.db.enums.AvailableInstrumentStatus;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;
import ru.webinnovations.template_web_project.db.models.Instrument;
import ru.webinnovations.template_web_project.db.models.Portfolio;
import ru.webinnovations.template_web_project.db.models.MoneyValueModel;
import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.db.repository.InstrumentRepository;
import ru.webinnovations.template_web_project.db.repository.StrategyRepository;
import ru.webinnovations.template_web_project.dto.InstrumentDTO;
import ru.webinnovations.template_web_project.dto.LastPriceDto;
import ru.webinnovations.template_web_project.dto.responces.InstrumentShortResponse;
import ru.webinnovations.template_web_project.services.InstrumentService;
import ru.webinnovations.template_web_project.services.UserService;
import ru.webinnovations.template_web_project.services.mapper.InstrumentMapper;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
@Slf4j
public class InstrumentServiceImpl implements InstrumentService {

    private final InstrumentRepository instrumentRepository;
    private final InstrumentMapper instrumentMapper;
    private final ContextProvider contextProvider;
    private final UserService userService;
    private final StrategyRepository strategyRepository;

    @Autowired
    public InstrumentServiceImpl(InstrumentRepository instrumentRepository,
                                 InstrumentMapper instrumentMapper,
                                 ContextProvider contextProvider,
                                 UserService userService, StrategyRepository strategyRepository) {
        this.instrumentRepository = instrumentRepository;
        this.instrumentMapper = instrumentMapper;
        this.contextProvider = contextProvider;
        this.userService = userService;
        this.strategyRepository = strategyRepository;
    }


    @Override
    @Transactional
    public void create() {
        InstrumentsService instrumentsService;
        List<InstrumentDTO> instrumentDTOsOld = instrumentMapper.toDto(instrumentRepository.findAll());
        List<InstrumentDTO> instrumentDTOsNew = new ArrayList<>();
        try {
            instrumentsService = contextProvider.getInvestApi().getInstrumentsService();
        } catch (Exception e) {
            log.error("Ошибка при обновлении списка инструментов: ", e);
            return;
        }

        instrumentDTOsNew.addAll(instrumentsService.getAllBonds().join()
                .stream()
                .filter(Bond::getApiTradeAvailableFlag)
                .map(instrumentMapper::mapFromBond)
                .collect(Collectors.toList()));

        instrumentDTOsNew.addAll(instrumentsService.getAllShares().join()
                .stream()
                .filter(Share::getApiTradeAvailableFlag)
                .map(instrumentMapper::mapFromShare)
                .collect(Collectors.toList()));

        instrumentDTOsNew.addAll(instrumentsService.getAllFutures().join()
                .stream()
                .filter(Future::getApiTradeAvailableFlag)
                .map(instrumentMapper::mapFromFuture)
                .collect(Collectors.toList()));

        instrumentDTOsNew.addAll(instrumentsService.getAllCurrencies().join()
                .stream()
                .filter(Currency::getApiTradeAvailableFlag)
                .map(instrumentMapper::mapFromCurrency)
                .collect(Collectors.toList()));

        instrumentDTOsNew.addAll(instrumentsService.getAllEtfs().join()
                .stream()
                .filter(Etf::getApiTradeAvailableFlag)
                .map(instrumentMapper::mapFromEtf)
                .collect(Collectors.toList()));

        instrumentDTOsNew.addAll(instrumentsService.getAllOptions().join()
                .stream()
                .filter(Option::getApiTradeAvailableFlag)
                .map(instrumentMapper::mapFromOption)
                .collect(Collectors.toList()));

        List<InstrumentDTO> mergedList = merge(instrumentDTOsOld, instrumentDTOsNew);

        instrumentRepository.saveAll(instrumentMapper.toEntity(mergedList));
    }

    private InvestApi getInvestApi() {
        try {
            return contextProvider.getInvestApi();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<InstrumentDTO> getAll() {
        List<Instrument> instruments = instrumentRepository.findAll();
        for (Instrument instrument : instruments) {
            instrument.setIsAvailable(getInstrumentStatus(instrument.getUid()));
            instrument.setLastPrice(toLastPriceStr(getInstrumentLastPrice(instrument.getUid())));
        }
        return instruments.stream().map(instrumentMapper::toDto).collect(Collectors.toList());
    }

    private boolean getInstrumentStatus(String instrumentUid) {
        MarketDataService marketDataService;
        GetTradingStatusResponse tradingStatus;
        List<AvailableInstrumentStatus> status = new ArrayList<>(EnumSet.allOf(AvailableInstrumentStatus.class));
        List<String> statusValue = status.stream().map(AvailableInstrumentStatus::name).collect(Collectors.toList());
        try {
            marketDataService = contextProvider.getInvestApi().getMarketDataService();
            tradingStatus = marketDataService.getTradingStatus(instrumentUid).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return statusValue.contains(tradingStatus.getTradingStatus().toString());
    }

    @Override
    public List<LastPriceDto> getLastPricesByInstrumentId(String instrumentId, LocalDateTime from,
                                                          LocalDateTime to, CandleInterval candleInterval) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        List<LastPriceDto> result = new ArrayList<>();
        InvestApi investApi;
        try {
            investApi = contextProvider.getInvestApi(user);
        } catch (Exception e) {
            log.error("Error while getting last prices by instrument: {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        MarketDataService marketDataService = investApi.getMarketDataService();

        List<HistoricCandle> candles;
        try {
            candles = marketDataService.getCandles(instrumentId,
                    from.atZone(ZoneId.of("Africa/Cairo")).toInstant(),
                    to.atZone(ZoneId.of("Africa/Cairo")).toInstant(),
                    candleInterval).get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("Error while getting candles: {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        candles.forEach(item -> result.add(LastPriceDto.builder()
                .openUnits(item.getOpen().getUnits())
                .openNano(item.getOpen().getNano())
                .closeUnits(item.getClose().getUnits())
                .closeNano(item.getClose().getNano())
                .highUnits(item.getHigh().getUnits())
                .highNano(item.getHigh().getNano())
                .lowUnits(item.getLow().getUnits())
                .lowNano(item.getLow().getNano())
                .timestamp(Instant
                        .ofEpochSecond(item.getTime().getSeconds(), item.getTime().getNanos())
                        .atZone(ZoneId.of("Africa/Cairo"))
                        .toLocalDateTime())
                .isPriceComplete(item.getIsComplete())
                .build()));

        return result;
    }

    private LastPrice getInstrumentLastPrice(String instrumentUid) {
        MarketDataService marketDataService;
        try {
            marketDataService = contextProvider.getInvestApi().getMarketDataService();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try {
            return marketDataService.getLastPrices(Collections.singleton(instrumentUid))
                    .get()
                    .stream()
                    .findFirst()
                    .orElse(null);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private String toLastPriceStr(LastPrice lastPrice) {
        return String.valueOf(lastPrice.getPrice().getUnits()).concat(".").concat(String.valueOf(lastPrice.getPrice().getNano()));
    }

    @Override
    public InstrumentDTO getInstrumentWithCandles(Long instrumentId, LocalDateTime from,
                                                  LocalDateTime to, CandleInterval candleInterval) {
        if (instrumentId == null || from == null || to == null || candleInterval == null) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User user = userService.findByUsername(authentication.getName());
            Sort sort = Sort.by(Sort.Order.asc("dateOfBegin"));
            List<ru.webinnovations.template_web_project.db.models.Strategy> strategies =
                    strategyRepository.getByUser(user, sort);
            if (strategies.isEmpty()) {
                return null;
            }
            ru.webinnovations.template_web_project.db.models.Strategy strategyDTO =
                    strategies.stream().reduce((first, second) -> second).get();
            return instrumentMapper.toDto(strategyDTO.getInstrument());
        }
        InstrumentDTO instrumentDTO = instrumentMapper.toDto(instrumentRepository.findById(instrumentId)
                .orElseThrow(EntityNotFoundException::new));
        instrumentDTO.setIsAvailable(getInstrumentStatus(instrumentDTO.getUid()));
        instrumentDTO.setLastPrice(toLastPriceStr(getInstrumentLastPrice(instrumentDTO.getUid())));
        instrumentDTO.setLastPriceList(getLastPricesByInstrumentId(instrumentDTO.getUid(), from, to, candleInterval));

        return instrumentDTO;
    }

    @Override
    public InstrumentDTO getById(Long id) {
        Instrument instrument = instrumentRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        //instrument.setIsAvailable(getInstrumentStatus(instrument.getUid()));
        //instrument.setLastPrice(toLastPriceStr(getInstrumentLastPrice(instrument.getUid())));
        return instrumentMapper.toDto(instrument);
    }

    @Override
    public Page<InstrumentDTO> searchByParameters(InstrumentTypeEnum instrumentType,
                                                  String name,
                                                  String ticker,
                                                  int offset,
                                                  int pageNumber,
                                                  int pageSize,
                                                  boolean sorted,
                                                  boolean unpaged) {
        Pageable pageable = sorted ? PageRequest.of(offset, pageSize, Sort.by("name")) : PageRequest.of(offset, pageSize);
        pageable = pageable.withPage(pageNumber);
        if (unpaged) {
            pageable = Pageable.unpaged();
        }

        Page<Instrument> instruments = instrumentRepository.findByParameters(instrumentType, name, ticker, pageable);
        /*instruments.forEach(instrument -> {
            instrument.setIsAvailable(getInstrumentStatus(instrument.getUid()));
            instrument.setLastPrice(toLastPriceStr(getInstrumentLastPrice(instrument.getUid())));
        });*/

        return instruments.map(instrumentMapper::toDto);
    }

    @Override
    public Portfolio getPortfolio() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        if (user == null) {
            throw new EntityNotFoundException("Not found authenticated user");
        }
        InvestApi api;
        try {
            api = contextProvider.getInvestApi(user);
        } catch (Exception e) {
            log.error("Error while getting balance: {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        var accounts = api.getUserService().getAccountsSync();
        var mainAccount = accounts.get(0).getId();
        var portfolio = api.getOperationsService().getPortfolioSync(mainAccount);
        return Portfolio.builder()
                .totalAmountEtf(
                        MoneyValueModel.builder()
                                .value(portfolio.getTotalAmountEtfs().getValue())
                                .currency(portfolio.getTotalAmountEtfs().getCurrency())
                                .build())
                .totalAmountBonds(
                        MoneyValueModel.builder()
                                .value(portfolio.getTotalAmountBonds().getValue())
                                .currency(portfolio.getTotalAmountBonds().getCurrency())
                                .build()
                )
                .totalAmountCurrencies(
                        MoneyValueModel.builder()
                                .value(portfolio.getTotalAmountCurrencies().getValue())
                                .currency(portfolio.getTotalAmountCurrencies().getCurrency())
                                .build()
                )
                .totalAmountFutures(
                        MoneyValueModel.builder()
                                .value(portfolio.getTotalAmountFutures().getValue())
                                .currency(portfolio.getTotalAmountFutures().getCurrency())
                                .build()
                )
                .totalAmountShares(
                        MoneyValueModel.builder()
                                .value(portfolio.getTotalAmountShares().getValue())
                                .currency(portfolio.getTotalAmountShares().getCurrency())
                                .build()
                )
                .expectedYield(portfolio.getExpectedYield())
                .build();
    }


    @Override
    public int getQuantityExecutedOperations(String figi, LocalDateTime from, LocalDateTime to) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        InvestApi investApi;
        investApi = contextProvider.getInvestApi(user);
        var accounts = investApi.getUserService().getAccountsSync();
        var mainAccount = accounts.get(0).getId();
        var executedOperations = investApi.getOperationsService().getExecutedOperationsSync(
                mainAccount, from.atZone(ZoneId.of("Africa/Cairo")).toInstant(),
                to.atZone(ZoneId.of("Africa/Cairo")).toInstant(), figi
        );

        return executedOperations.size();
    }

    @Override
    public boolean existsByFigi(String figi) {
        return instrumentRepository.existsByFigi(figi);
    }

    @Override
    public List<InstrumentShortResponse> getInstrumentsFromTinkoffApi(InstrumentTypeEnum type) {
        InvestApi api = null;
        List<InstrumentShortResponse> result = new ArrayList<>();
        try {
            api = contextProvider.getInvestApi();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        switch (type) {
            case ETF: {
                var etfs = api.getInstrumentsService().getTradableEtfsSync();
                for (ru.tinkoff.piapi.contract.v1.Etf etf : etfs) {
                    result.add(InstrumentShortResponse.builder()
                            .uid(etf.getUid().toString())
                            .type(InstrumentTypeEnum.ETF)
                            .status(etf.getTradingStatus().toString())
                            .ticker(etf.getTicker().toString())
                            .figi(etf.getFigi().toString())
                            .build());
                }
                break;
            }
            case BOND: {
                var bonds = api.getInstrumentsService().getTradableBondsSync();
                for (ru.tinkoff.piapi.contract.v1.Bond bond : bonds) {
                    result.add(InstrumentShortResponse.builder()
                            .uid(bond.getUid().toString())
                            .type(InstrumentTypeEnum.BOND)
                            .status(bond.getTradingStatus().toString())
                            .ticker(bond.getTicker().toString())
                            .figi(bond.getFigi().toString())
                            .build());
                }

                break;
            }
            case SHARE: {
                var shares = api.getInstrumentsService().getTradableSharesSync();
                for (ru.tinkoff.piapi.contract.v1.Share share : shares) {
                    result.add(InstrumentShortResponse.builder()
                            .uid(share.getUid().toString())
                            .type(InstrumentTypeEnum.SHARE)
                            .status(share.getTradingStatus().toString())
                            .ticker(share.getTicker().toString())
                            .figi(share.getFigi().toString())
                            .build());
                }

                break;
            }
            case FUTURE: {
                var futures = api.getInstrumentsService().getTradableFuturesSync();
                for (ru.tinkoff.piapi.contract.v1.Future future : futures) {
                    result.add(InstrumentShortResponse.builder()
                            .uid(future.getUid().toString())
                            .type(InstrumentTypeEnum.FUTURE)
                            .status(future.getTradingStatus().toString())
                            .ticker(future.getTicker().toString())
                            .figi(future.getFigi().toString())
                            .build());
                }

                break;
            }
            case OPTION: {
                var options = api.getInstrumentsService().getTradableOptionsSync();
                for (ru.tinkoff.piapi.contract.v1.Option option : options) {
                    result.add(InstrumentShortResponse.builder()
                            .uid(option.getUid())
                            .type(InstrumentTypeEnum.OPTION)
                            .status(option.getTradingStatus().toString())
                            .ticker(option.getTicker())
                            .figi("Not_figi".concat(option.getUid()))
                            .build());
                }
                break;
            }
            case CURRENCY: {
                var currencies = api.getInstrumentsService().getTradableCurrenciesSync();
                for (ru.tinkoff.piapi.contract.v1.Currency currency : currencies) {
                    result.add(InstrumentShortResponse.builder()
                            .uid(currency.getUid().toString())
                            .type(InstrumentTypeEnum.CURRENCY)
                            .status(currency.getTradingStatus().toString())
                            .ticker(currency.getTicker().toString())
                            .figi(currency.getFigi().toString())
                            .build());
                }
                break;
            }
        }
        return result;
    }

    @Override
    public void deleteByType(InstrumentTypeEnum type) {
        List<Instrument> instruments = instrumentRepository.findAllByType(type);
        instrumentRepository.deleteAll(instruments);
        instruments = instrumentRepository.findAllByType(type);
        if(!instruments.isEmpty()){
            deleteByType(type);
        }
    }


    @Override
    public Page<InstrumentDTO> searchByParameters(InstrumentTypeEnum instrumentType, String name,
                                                  String ticker, Pageable pageable) {
        Page<Instrument> instruments = instrumentRepository.findByParameters(instrumentType, name, ticker, pageable);
        return instruments.map(instrumentMapper::toDto);
    }

    private List<InstrumentDTO> merge(List<InstrumentDTO> oldDTOs, List<InstrumentDTO> newDTOs) {
        List<InstrumentDTO> mergedDTOs = new ArrayList<>();

        for (InstrumentDTO newDTO : newDTOs) {
            InstrumentDTO oldDTO = oldDTOs.stream()
                    .filter(item -> item.getUid().equals(newDTO.getUid()))
                    .findFirst().orElse(null);

            if (oldDTO == null) {
                mergedDTOs.add(newDTO);
                continue;
            }

            if (!oldDTO.equals(newDTO)) {
                oldDTO.setType(newDTO.getType());
                oldDTO.setFigi(newDTO.getFigi());
                oldDTO.setTicker(newDTO.getTicker());
                oldDTO.setNominal(newDTO.getNominal());
                oldDTO.setMinPriceIncrement(newDTO.getMinPriceIncrement());
                oldDTO.setIsin(newDTO.getIsin());
                oldDTO.setName(newDTO.getName());
                oldDTO.setLot(newDTO.getLot());
                oldDTO.setCurrency(newDTO.getCurrency());
                oldDTO.setPositionUid(newDTO.getPositionUid());
                oldDTO.setPrice(newDTO.getPrice());
                oldDTO.setMinPriceIncrementAmount(newDTO.getMinPriceIncrementAmount());

                mergedDTOs.add(oldDTO);
            }
        }

        return mergedDTOs;
    }




}
