package ru.webinnovations.template_web_project.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.webinnovations.template_web_project.db.models.Role;
import ru.webinnovations.template_web_project.db.repository.RoleRepository;
import ru.webinnovations.template_web_project.dto.requests.RoleRequest;
import ru.webinnovations.template_web_project.dto.responces.RoleResponse;
import ru.webinnovations.template_web_project.services.RoleService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    private RoleResponse buildRoleResponse(Role role) {
        return RoleResponse.builder()
                .name(role.getName())
                .id(role.getId())
                .build();
    }

    private Role buildRole(RoleRequest roleRequest) {
        Role role = new Role();
        role.setName(roleRequest.getName());
        return role;
    }

    private Role getOne(Long id) {
        return roleRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Not found role by id " + id)
        );
    }

    @Override
    public RoleResponse getRole(Long id) {
        return buildRoleResponse(getOne(id));
    }

    @Override
    public RoleResponse getRole(String name) {
        return buildRoleResponse(roleRepository.findByName(name).orElseThrow(
                () -> new EntityNotFoundException("Not found role by name " + name)
        ));
    }

    @Override
    public List<RoleResponse> getRoles() {
        return roleRepository.findAll().parallelStream().map(this::buildRoleResponse).collect(Collectors.toList());
    }

    @Override
    public RoleResponse createRole(RoleRequest roleRequest) {
        return buildRoleResponse(roleRepository.save(buildRole(roleRequest)));
    }


    @Override
    public RoleResponse updateRole(Long id, RoleRequest roleRequest) {
        Role role = getOne(id);
        role.setName(roleRequest.getName());

        return buildRoleResponse(roleRepository.save(role));
    }
}
