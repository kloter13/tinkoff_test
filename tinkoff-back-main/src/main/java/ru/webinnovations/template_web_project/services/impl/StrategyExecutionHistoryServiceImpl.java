package ru.webinnovations.template_web_project.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;
import ru.webinnovations.template_web_project.db.repository.StrategyExecutionHistoryRepository;
import ru.webinnovations.template_web_project.dto.StrategyExecutionHistoryDTO;
import ru.webinnovations.template_web_project.services.StrategyExecutionHistoryService;
import ru.webinnovations.template_web_project.services.mapper.StrategyExecutionHistoryMapper;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class StrategyExecutionHistoryServiceImpl implements StrategyExecutionHistoryService {
    private final StrategyExecutionHistoryRepository executionHistoryRepository;
    private final StrategyExecutionHistoryMapper strategyExecutionHistoryMapper;

    @Autowired
    public StrategyExecutionHistoryServiceImpl(StrategyExecutionHistoryRepository executionHistoryRepository,
                                               StrategyExecutionHistoryMapper strategyExecutionHistoryMapper) {
        this.executionHistoryRepository = executionHistoryRepository;
        this.strategyExecutionHistoryMapper = strategyExecutionHistoryMapper;
    }

    @Override
    public StrategyExecutionHistoryDTO create(StrategyExecutionHistoryDTO request) {
        StrategyExecutionHistory strategyExecutionHistory = executionHistoryRepository
                .save(strategyExecutionHistoryMapper.toEntity(request));
        return strategyExecutionHistoryMapper.toDto(strategyExecutionHistory);
    }

    @Override
    public StrategyExecutionHistoryDTO update(StrategyExecutionHistoryDTO strategyExecutionHistoryDTO) {
        StrategyExecutionHistory strategyExecutionHistory = executionHistoryRepository.findById(strategyExecutionHistoryDTO.getId()).orElseThrow(
                () -> new EntityNotFoundException("Not found strategy execution history by id")
        );

        return strategyExecutionHistoryMapper.toDto(executionHistoryRepository
                .save(strategyExecutionHistoryMapper
                        .toEntity(strategyExecutionHistoryDTO)));
    }

    @Override
    public List<StrategyExecutionHistoryDTO> getStrategyExecutionHistoryByStrategy(Long strategyId) {
        return strategyExecutionHistoryMapper.toDto(executionHistoryRepository.findAll());
    }

    @Override
    public void delete(Long id) {
        StrategyExecutionHistory strategyExecutionHistory = executionHistoryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found strategy execution history by id " + id));
        executionHistoryRepository.delete(strategyExecutionHistory);
    }

    @Override
    public StrategyExecutionHistory getByOrderId(String orderId) {
        return executionHistoryRepository.findByOrderId(orderId).orElseThrow(
                ()-> new EntityNotFoundException("Not found history by order id "+orderId)
        );
    }

    @Override
    public StrategyExecutionHistory update(StrategyExecutionHistory strategyExecutionHistory, Long id) {
        StrategyExecutionHistory history = executionHistoryRepository.findById(id).orElseThrow(
                ()->new EntityNotFoundException("Not found history by id "+id)
        );
        history.setStrategy(strategyExecutionHistory.getStrategy());
        history.setVolume(strategyExecutionHistory.getVolume());
        history.setStatus(strategyExecutionHistory.getStatus());
        history.setType(strategyExecutionHistory.getType());
        history.setExecutionTimestamp(strategyExecutionHistory.getExecutionTimestamp());
        history.setOrderId(strategyExecutionHistory.getOrderId());
        return executionHistoryRepository.save(history);
    }
}
