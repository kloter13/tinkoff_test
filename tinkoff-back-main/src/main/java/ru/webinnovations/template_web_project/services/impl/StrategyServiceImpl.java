package ru.webinnovations.template_web_project.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.piapi.contract.v1.*;
import ru.tinkoff.piapi.core.InvestApi;
import ru.tinkoff.piapi.core.MarketDataService;
import ru.tinkoff.piapi.core.OrdersService;
import ru.tinkoff.piapi.core.exception.ApiRuntimeException;
import ru.webinnovations.template_web_project.configs.ContextProvider;
import ru.webinnovations.template_web_project.db.enums.*;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.db.models.*;
import ru.webinnovations.template_web_project.db.repository.StrategyRepository;
import ru.webinnovations.template_web_project.dto.InstrumentDTO;
import ru.webinnovations.template_web_project.dto.StrategyDTO;
import ru.webinnovations.template_web_project.exceptions.InstrumentNotAvailableException;
import ru.webinnovations.template_web_project.services.InstrumentService;
import ru.webinnovations.template_web_project.services.StrategyExecutionHistoryService;
import ru.webinnovations.template_web_project.services.StrategyService;
import ru.webinnovations.template_web_project.services.UserService;
import ru.webinnovations.template_web_project.services.mapper.StrategyMapper;
import ru.webinnovations.template_web_project.utils.StrategyUtil;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StrategyServiceImpl implements StrategyService {
    private final StrategyRepository strategyRepository;

    private final ContextProvider contextProvider;

    private final StrategyMapper strategyMapper;

    private final UserService userService;

    private final InstrumentService instrumentService;

    private final StrategyUtil strategyUtil;
    private final StrategyExecutionHistoryService strategyExecutionHistoryService;

    private final TinkoffDefaultTokenServiceImpl tinkoffDefaultTokenService;


    @Autowired
    public StrategyServiceImpl(StrategyRepository strategyRepository,
                               ContextProvider contextProvider,
                               StrategyMapper strategyMapper,
                               UserService userService,
                               InstrumentService instrumentService,
                               StrategyUtil strategyUtil,
                               StrategyExecutionHistoryService strategyExecutionHistoryService,
                               TinkoffDefaultTokenServiceImpl tinkoffDefaultTokenService) {
        this.strategyRepository = strategyRepository;
        this.contextProvider = contextProvider;
        this.strategyMapper = strategyMapper;
        this.userService = userService;
        this.instrumentService = instrumentService;
        this.strategyUtil = strategyUtil;
        this.strategyExecutionHistoryService = strategyExecutionHistoryService;
        this.tinkoffDefaultTokenService = tinkoffDefaultTokenService;
    }

    @Override
    @Transactional
    public StrategyDTO create(StrategyDTO strategyDTO) {
        if (!isAvailableInstrument(strategyDTO.getInstrumentDTO().getUid())) {
            throw new InstrumentNotAvailableException("Инструмент не доступен для торгов");
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        MarketDataService marketDataService;
        LastPrice lastPrice;
        try {
            marketDataService = contextProvider.getInvestApi(user).getMarketDataService();
            lastPrice = marketDataService.getLastPrices(Collections.singleton(strategyDTO.getInstrumentDTO().getUid())).get().stream().findFirst().orElse(null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        strategyDTO.setPauseBuy(false);
        strategyDTO.setPauseSell(false);
        strategyDTO.setPriceChanged(true);
        if (strategyDTO.getOrderType() == null) {
            strategyDTO.setOrderType(OrderTypeEnum.ORDER_TYPE_MARKET);
        }
        Strategy strategy = strategyMapper.toEntity(strategyDTO);
        strategy.setExecutedOperation(0);
        strategy.setUser(user);
        assert lastPrice != null;
        strategyDTO.getInstrumentDTO().setLastPrice(lastPrice.getPrice().toString());
        strategy.setStatus(StrategyExecutionStatus.IN_PROGRESS);


        return strategyMapper.toDto(strategyRepository.save(strategy));
    }

    private Strategy getOne(Long id) {
        return strategyRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Not found strategy by id " + id)
        );
    }

    public boolean isAvailableInstrument(String instrumentUid) {
        List<AvailableInstrumentStatus> status = new ArrayList<>(EnumSet.allOf(AvailableInstrumentStatus.class));
        List<String> statusValue = status.stream().map(AvailableInstrumentStatus::name).collect(Collectors.toList());
        MarketDataService marketDataService;
        GetTradingStatusResponse tradingStatus;
        try {
            marketDataService = contextProvider.getInvestApi().getMarketDataService();
            tradingStatus = marketDataService.getTradingStatus(instrumentUid).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return statusValue.contains(tradingStatus.getTradingStatus().toString());
    }


    @Override
    public void getSchedule(String instrumentFigi) {
        InvestApi api = strategyUtil.getInvestApi();
        var currencies = api.getInstrumentsService().getTradableCurrenciesSync();
        var bonds = api.getInstrumentsService().getTradableBondsSync();
        var etfs = api.getInstrumentsService().getTradableEtfsSync();
        var futures = api.getInstrumentsService().getTradableFuturesSync();
        for (ru.tinkoff.piapi.contract.v1.Bond bond : bonds) {
            if (!bond.getTradingStatus().toString().equals("SECURITY_TRADING_STATUS_NOT_AVAILABLE_FOR_TRADING")) {
                log.info("available {}", bond.getTradingStatus());
            }
        }
        for (ru.tinkoff.piapi.contract.v1.Etf etf : etfs) {
            if (!etf.getTradingStatus().toString().equals("SECURITY_TRADING_STATUS_NOT_AVAILABLE_FOR_TRADING")) {
                log.info("available {}", etf.getTradingStatus());
            }
        }
        for (ru.tinkoff.piapi.contract.v1.Future future : futures) {
            if (!future.getTradingStatus().toString().equals("SECURITY_TRADING_STATUS_NOT_AVAILABLE_FOR_TRADING")) {
                log.info("available {}", future.getTradingStatus());
            }
        }
        for (ru.tinkoff.piapi.contract.v1.Currency currency : currencies) {
            if (!currency.getTradingStatus().toString().equals("SECURITY_TRADING_STATUS_NOT_AVAILABLE_FOR_TRADING")) {
                log.info("available {}", currency.getTradingStatus());
            }
        }

    }

    @Override
    public OrderStateModel getOrderState(String orderId) {
        return strategyUtil.getOrderState(orderId);
    }


    @Override
    @Transactional
    public StrategyDTO update(StrategyDTO strategyDTO, Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        Strategy strategy = strategyRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Такой стратегии не существует")
        );
        strategy.setStatus(strategyDTO.getStatus());
        strategy.setDateOfEnd(strategyDTO.getDateOfEnd());
        strategy.setPriceChanged(true);
        if (strategyDTO.getOrderType() != null) {
            strategy.setOrderType(strategyDTO.getOrderType());
        } else {
            strategy.setOrderType(OrderTypeEnum.ORDER_TYPE_MARKET);
        }
        strategy.setUser(user);
        return strategyMapper.toDto(strategyRepository.save(strategy));
    }

    @Override
    public StrategyDTO getById(Long id) {
        StrategyDTO strategyDTO = strategyMapper.toDto(strategyRepository.findById(id).orElseThrow(EntityNotFoundException::new));
        InstrumentDTO instrumentDTO = strategyDTO.getInstrumentDTO();
        instrumentDTO.setLastPriceList(instrumentService.getLastPricesByInstrumentId(instrumentDTO.getUid(), LocalDate.now(ZoneId.of("Africa/Cairo")).atStartOfDay().minusDays(1), LocalDate.now(ZoneId.of("Africa/Cairo")).atStartOfDay(), CandleInterval.CANDLE_INTERVAL_HOUR));
        strategyDTO.setExecutedOperation(Math.toIntExact(getTotalExecutedLots(strategyMapper.toEntity(strategyDTO))));
        strategyDTO.setInstrumentDTO(instrumentDTO);
        return strategyDTO;
    }

    @Override
    public List<StrategyDTO> getAll() {
        List<Strategy> strategies = strategyRepository.findAll();
        strategies.forEach(strategy -> strategy.setExecutedOperation(Math.toIntExact(getTotalExecutedLots(strategy))));
        return strategyMapper.toDto(strategies);
    }

    @Override
    public List<StrategyDTO> getByUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        Sort sort = Sort.by(Sort.Order.asc("dateOfBegin"));
        List<Strategy> strategies = strategyRepository.getByUser(user, sort);
        for (Strategy strategy : strategies) {
            strategy.setExecutedOperation(Math.toIntExact(getTotalExecutedLots(strategy)));
        }
        return strategyMapper.toDto(strategies);
    }

    private boolean allExecuted(Strategy strategy, StrategyExecutionType type) {
        if (existsStrategyHistories(strategy)) {
            int count = countHistoryByType(strategy, type);
            int countExecutedOrder = 0;
            List<StrategyExecutionHistory> histories = strategy.getHistory();
            for (StrategyExecutionHistory history : histories) {
                if (history.getType().equals(type)) {
                    if (isCompletedOrder(history)) {
                        countExecutedOrder++;
                    } else {
                        OrderStateModel orderState = strategyUtil.getOrderState(history.getOrderId());
                        assert orderState != null;
                        if (orderState.allExecuted()) {
                            log.info("updating history {} with volume {}", history.getId(), history.getVolume());
                            countExecutedOrder++;
                            history.setStatus(StrategyOrderStatus.EXECUTION_REPORT_STATUS_FILL);
                            history.setVolume(orderState.getLotsExecuted());
                        }
                    }
                }
            }
            return count == countExecutedOrder && count != 0;
        } else return type.equals(StrategyExecutionType.BUY);
    }

    private boolean isCompletedOrder(StrategyExecutionHistory history) {
        return history.getStatus().equals(StrategyOrderStatus.EXECUTION_REPORT_STATUS_FILL);
    }


    private int countHistoryByType(Strategy strategy, StrategyExecutionType type) {
        int count = 0;
        List<StrategyExecutionHistory> histories = strategy.getHistory();
        for (StrategyExecutionHistory history : histories) {
            if (history.getType().equals(type)) {
                count++;
            }
        }
        return count;
    }


    @Override
    public long getTotalExecutedLots(Strategy strategy) {
        var result = 0L;
        if (existsStrategyHistories(strategy)) {
            var histories = strategy.getHistory();
            for (StrategyExecutionHistory history : histories) {
                result = result + getExecutedLots(history.getOrderId());
            }
        }
        return result;
    }

    @Override
    public void launchPausedStrategy() {
        List<Strategy> pausedStrategies = strategyRepository
                .findAll()
                .stream()
                .filter(item -> item.getStatus().equals(StrategyExecutionStatus.FAILURE))
                .collect(Collectors.toList());
        for (Strategy strategy : pausedStrategies) {
            if (existsStrategyHistories(strategy))
                strategy.setStatus(StrategyExecutionStatus.PARTLY);
            else
                strategy.setStatus(StrategyExecutionStatus.IN_PROGRESS);
            strategyRepository.save(strategy);
        }
    }

    @Override
    public void testDoubleComparison() {
        Quotation orderPrice = toQuotation("98.90");
        MarketDataService marketDataService;
        InvestApi api = strategyUtil.getInvestApi();
        marketDataService = api.getMarketDataService();
        LastPrice lastPrice = null;
        try {
            lastPrice = marketDataService.getLastPrices(Collections
                    .singleton("cbdf1d32-5758-490e-a2b1-780eaa79bdf7")).get().stream().findFirst().orElse(null);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        assert lastPrice != null;
        log.info("can buy {}", canBuy(toBigDecimal(orderPrice), toBigDecimal(lastPrice)));
        log.info("can sell {}", canSell(toBigDecimal(orderPrice), toBigDecimal(lastPrice)));
    }

    @Override
    public void unpauseStrategies() {
        List<Strategy> pausedStrategies = strategyRepository
                .findAll()
                .stream()
                .filter(item -> item.isPauseBuy() || item.isPauseSell())
                .collect(Collectors.toList());
        for (Strategy strategy : pausedStrategies) {
            strategy.setPauseBuy(false);
            strategy.setPauseSell(false);
            strategyRepository.save(strategy);
        }
    }


    private long getExecutedLots(String orderId) {
        StrategyExecutionHistory history = strategyExecutionHistoryService.getByOrderId(orderId);
        if (isCompletedOrder(history)) {
            return history.getVolume();
        }
        return strategyUtil.getOrderState(orderId).getLotsExecuted();
    }

    private long getRequestedLots(String orderId) {
        StrategyExecutionHistory history = strategyExecutionHistoryService.getByOrderId(orderId);
        if (isCompletedOrder(history)) {
            return history.getVolume();
        }
        return strategyUtil.getOrderState(orderId).getLotsRequested();
    }


    private boolean existsStrategyHistories(Strategy strategy) {
        return strategy.getHistory() != null && !strategy.getHistory().isEmpty();
    }

    private long getSellVolume(Strategy strategy) {
        var result = 0L;
        if (existsStrategyHistories(strategy)) {
            var buyExecutedLots = 0L;
            for (StrategyExecutionHistory history : strategy.getHistory()) {
                if (isBuyStrategy(history)) {
                    buyExecutedLots = buyExecutedLots + getExecutedLots(history.getOrderId());
                } else {
                    result = result + getRequestedLots(history.getOrderId());
                }
            }
            log.info("sell volume {} id {}", buyExecutedLots - result, strategy.getVolume());
            return buyExecutedLots - result;
        }
        log.info("no volume to sell {}", strategy.getId());
        return result;
    }

    private boolean isBuyStrategy(StrategyExecutionHistory history) {
        return history.getType().equals(StrategyExecutionType.BUY);
    }

    private boolean canSell(Strategy strategy) {
        return getSellVolume(strategy) > 0L;
    }


    private void updateStrategy(Long strategyId) {
        Strategy strategy = getOne(strategyId);
        strategy.setExecutedOperation(Math.toIntExact(getTotalExecutedLots(strategy)));
        if (allExecuted(strategy, StrategyExecutionType.BUY)
                && allExecuted(strategy, StrategyExecutionType.SELL)) {
            strategy.setStatus(StrategyExecutionStatus.IN_PROGRESS);
        }
        strategyRepository.save(strategy);
    }


    @Override
    @Transactional
    public void checkAndExecuteStrategy() {
        log.info("checking and executing strategy");

        List<Strategy> activeStrategies = strategyRepository
                .findAll()
                .stream()
                .filter(item -> item.getStatus().equals(StrategyExecutionStatus.IN_PROGRESS) || item.getStatus().equals(StrategyExecutionStatus.PARTLY))
                .collect(Collectors.toList());
        OrdersService ordersService;
        MarketDataService marketDataService;
        Long id = 1L;
        InvestApi api = null;
        try {
            api = contextProvider.getInvestApi();
            ordersService = api.getOrdersService();
            marketDataService = api.getMarketDataService();
            for (Strategy strategy : activeStrategies) {

                if (strategy.getDateOfBegin() == null || strategy.getDateOfEnd().isBefore(LocalDateTime.now()) || strategy.getDateOfBegin().isAfter(LocalDateTime.now())) {
                    strategy.setStatus(StrategyExecutionStatus.EXPIRED);
                    strategyRepository.save(strategy);
                    continue;
                }

                LastPrice lastPrice = marketDataService.getLastPrices(Collections.singleton(strategy.getInstrument().getUid())).get().stream().findFirst().orElse(null);

                if (lastPrice == null) {
                    continue;
                }

                String accountId;
                id = strategy.getId();
                InvestApi investApi = contextProvider.getInvestApi(strategy.getUser());
                Account account = investApi.getUserService().getAccounts().join().get(0);
                accountId = account.getId();

                if (strategy.isPauseBuy()) {
                    log.info("strategy paused to buy {} ", id);
                } else {
                    buyOrder(strategy, api, lastPrice, ordersService, accountId);
                }
                if (strategy.isPauseSell()) {
                    log.info("strategy paused to sell {} ", id);
                } else {
                    sellOrder(strategy, api, lastPrice, ordersService, accountId);
                }

                updateStrategy(id);
            }
            strategyRepository.saveAll(activeStrategies);

        } catch (ExecutionException | InterruptedException |
                 ApiRuntimeException e) {
            createApiWithDefaultDBToken(api);
            if (api != null) {
                pausesStrategy(id, e.getMessage());
            }
            log.error("Ошибка при получении цен последний сделок по инструментам: ", e);
        } catch (Exception e) {
            createApiWithDefaultDBToken(api);
            if (api != null) {
                pausesStrategy(id, e.getMessage());
            }
            log.error("Ошибка при получении инвестиционных сервисов: ", e);
        }

    }

    private void createApiWithDefaultDBToken(InvestApi api) {
        if (api == null) {
            String defaultToken = tinkoffDefaultTokenService.getDefaultToken();
            if (defaultToken != null && !defaultToken.isBlank()) {
                User user = new User();
                user.setToken(defaultToken);
                user.setIsTokenCreated(true);
                contextProvider.getInvestApi(user);
            }
        }
    }

    private void pausesStrategy(Long id, String cause) {
        Strategy strategy = strategyRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Not found strategy by id " + id)
        );
        if (isInsufficientFond(cause)) {
            if (strategy.isPauseBuy()) {
                strategy.setPauseBuy(false);
                strategy.setPauseSell(true);
                log.info("paused order to sel strategy id {} cause: {} ", id, cause);
            } else {
                strategy.setPauseSell(false);
                strategy.setPauseBuy(true);
                log.info("paused order to buy strategy id {} cause: {} ", id, cause);
            }
        } else {

            log.info("paused strategy id {} cause: {} ", id, cause);
            strategy.setStatus(StrategyExecutionStatus.FAILURE);
        }
        strategyRepository.save(strategy);
    }

    private boolean isInsufficientFond(String exceptionCause) {
        return exceptionCause.contains("Недостаточно активов для маржинальной сделки");
    }

    private OrderType getOrderType(OrderTypeEnum strategyOrderType) {
        switch (strategyOrderType) {
            case ORDER_TYPE_LIMIT:
                return OrderType.ORDER_TYPE_LIMIT;
            case ORDER_TYPE_UNSPECIFIED:
                return OrderType.ORDER_TYPE_UNSPECIFIED;
            case ORDER_TYPE_BESTPRICE:
                return OrderType.ORDER_TYPE_BESTPRICE;
            default:
                return OrderType.ORDER_TYPE_MARKET;
        }
    }

    private void buyOrder(Strategy strategy, InvestApi api, LastPrice lastPrice, OrdersService ordersService, String accountId) throws ExecutionException, InterruptedException {
        Quotation orderBuyPrice = toQuotation(strategy.getBuyPrice());
        log.info("order buy user price {}, instrument last price {} ", orderBuyPrice, lastPrice.getPrice());
        if (canBuy(toBigDecimal(orderBuyPrice), toBigDecimal(lastPrice))) {
            log.info("trying to buy {}.{} ", orderBuyPrice.getUnits(), orderBuyPrice.getNano());
            if ((allExecuted(strategy, StrategyExecutionType.SELL)
                    || countHistoryByType(strategy, StrategyExecutionType.SELL) == 0)
                    && isAvailableInstrument(strategy.getInstrument().getUid())
                    && strategy.isPriceChanged()) {
                log.info("posting order to buy");
                PostOrderResponse response = ordersService.postOrder(strategy.getInstrument().getFigi(),
                        strategy.getVolume(),
                        orderBuyPrice,
                        OrderDirection.ORDER_DIRECTION_BUY,
                        accountId,
                        getOrderType(strategy.getOrderType()),
                        UUID.randomUUID().toString()).get();

                List<StrategyExecutionHistory> histories = strategy.getHistory() == null ? new ArrayList<>() : strategy.getHistory();

                histories.add(StrategyExecutionHistory.builder().executionTimestamp(LocalDateTime.now())
                        .type(StrategyExecutionType.BUY)
                        .volume(response.getLotsExecuted())
                        .strategy(strategy)
                        .status(StrategyOrderStatus
                                .valueOf(response.getExecutionReportStatus().toString()))
                        .orderId(response.getOrderId()).build());

                strategy.setHistory(histories);
                strategy.setStatus(StrategyExecutionStatus.PARTLY);
                strategy.setPauseSell(false);
                strategyRepository.save(strategy);
                strategy.setPriceChanged(false);
                log.info("posted order to buy");
            } else {
                log.info("can't buy we need to sell all for this strategy or instrument is not available");
            }

        } else {
            if (!strategy.isPriceChanged()) {
                strategy.setPriceChanged(true);
                log.info("strategy {} price changed {}", strategy.getId(), toBigDecimal(lastPrice));
                strategyRepository.save(strategy);
            }
        }
    }


    private void sellOrder(Strategy strategy, InvestApi api, LastPrice lastPrice, OrdersService ordersService, String accountId) throws ExecutionException, InterruptedException {
        Quotation orderSellPrice = toQuotation(strategy.getSellPrice());
        log.info("order sell user price {}, instrument last price {} ", orderSellPrice, lastPrice.getPrice());
        if (canSell(toBigDecimal(orderSellPrice), toBigDecimal(lastPrice))) {
            log.info("trying to sell {} {}", orderSellPrice.getUnits(), orderSellPrice.getNano());
            if (canSell(strategy) && isAvailableInstrument(strategy.getInstrument().getUid())) {
                var sellVolume = getSellVolume(strategy);
                PostOrderResponse response = ordersService.postOrder(strategy.getInstrument().getFigi(),
                        sellVolume,
                        orderSellPrice,
                        OrderDirection.ORDER_DIRECTION_SELL,
                        accountId,
                        getOrderType(strategy.getOrderType()),
                        UUID.randomUUID().toString()).get();

                List<StrategyExecutionHistory> histories = strategy.getHistory() == null ? new ArrayList<>() : strategy.getHistory();

                histories.add(StrategyExecutionHistory.builder()
                        .executionTimestamp(LocalDateTime.now())
                        .type(StrategyExecutionType.SELL)
                        .volume(response.getLotsExecuted())
                        .strategy(strategy)
                        .status(StrategyOrderStatus
                                .valueOf(response.getExecutionReportStatus().toString())).orderId(response.getOrderId()).build());

                strategy.setHistory(histories);
                strategy.setStatus(StrategyExecutionStatus.PARTLY);
                strategy.setPauseBuy(false);
                strategyRepository.save(strategy);
                log.info("posted order to sell");
            } else {
                log.info("can't sell! firstly buy or instrument is not available");
            }
        }
    }

    private BigDecimal toBigDecimal(Quotation quotation) {
        return quotation.getUnits() == 0 && quotation.getNano() == 0 ? BigDecimal.ZERO :
                BigDecimal.valueOf(quotation.getUnits()).add(BigDecimal.valueOf(quotation.getNano(), 9));
    }

    private Quotation toQuotation(String strPrice) {
        BigDecimal value = new BigDecimal(strPrice);
        return Quotation.newBuilder()
                .setUnits(value != null ? value.longValue() : 0)
                .setNano(value != null ? value.remainder(BigDecimal.ONE).multiply(BigDecimal.valueOf(1_000_000_000)).intValue() : 0)
                .build();
    }

    private boolean canBuy(BigDecimal userOrderPrice, BigDecimal instrumentLastPrice) {
        return userOrderPrice.compareTo(instrumentLastPrice) >= 0;
    }

    private boolean canSell(BigDecimal userOrderPrice, BigDecimal instrumentLastPrice) {
        return userOrderPrice.compareTo(instrumentLastPrice) <= 0;
    }

    private BigDecimal toBigDecimal(LastPrice lastPrice) {
        return toBigDecimal(toQuotation(String
                .valueOf(lastPrice.getPrice().getUnits()).concat(".").concat(
                        String.valueOf(lastPrice.getPrice().getNano())
                )));
    }


    @Override
    @Transactional
    public void deleteById(Long id) {
        Strategy strategy = strategyRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<Strategy> strategies = strategy.getUser().getStrategies();
        strategies.clear();
        strategy.getUser().setStrategies(null);
        strategyRepository.delete(strategy);
    }


    @Override
    public MarginAttributeModel getMarginAttributes() {
        InvestApi api = strategyUtil.getInvestApi();
        var mainAccount = api.getUserService().getAccountsSync().get(0).getId();
        GetMarginAttributesResponse request = null;
        try {
            request = api.getUserService().getMarginAttributes(mainAccount).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        return MarginAttributeModel
                .builder()
                .liquidPortfolio(strategyUtil.convertMoneyValue(request.getLiquidPortfolio()))
                .startingMargin(strategyUtil.convertMoneyValue(request.getStartingMargin()))
                .minimalMargin(strategyUtil.convertMoneyValue(request.getMinimalMargin()))
                .missingFundsAmount(strategyUtil.convertMoneyValue(request.getAmountOfMissingFunds()))
                .fundSufficiencyLevel(convertQuotation(request.getFundsSufficiencyLevel()))
                .correctedMargin(strategyUtil.convertMoneyValue(request.getCorrectedMargin()))
                .build();
    }


    private QuotationValue convertQuotation(Quotation quotation) {
        return QuotationValue.builder()
                .value(BigDecimal.valueOf(
                        quotation.getUnits()
                ).add(new BigDecimal("0.".concat(String.valueOf(quotation.getNano())))))
                .build();
    }


}
