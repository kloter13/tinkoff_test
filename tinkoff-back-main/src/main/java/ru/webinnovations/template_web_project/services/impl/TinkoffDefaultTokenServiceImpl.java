package ru.webinnovations.template_web_project.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.webinnovations.template_web_project.db.models.TinkoffDefaultToken;
import ru.webinnovations.template_web_project.db.repository.TinkoffDefaultTokenRepository;
import ru.webinnovations.template_web_project.services.TinkoffDefaultTokenService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TinkoffDefaultTokenServiceImpl implements TinkoffDefaultTokenService {
    private final TinkoffDefaultTokenRepository tinkoffDefaultTokenRepository;

    @Autowired
    public TinkoffDefaultTokenServiceImpl(TinkoffDefaultTokenRepository tinkoffDefaultTokenRepository) {
        this.tinkoffDefaultTokenRepository = tinkoffDefaultTokenRepository;
    }

    private TinkoffDefaultToken getOne(Integer id) {
        return tinkoffDefaultTokenRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found default token by id " + id));
    }

    @Override
    public List<TinkoffDefaultToken> getDefaultsToken() {
        return tinkoffDefaultTokenRepository.findAll();
    }

    @Override
    public void deleteDefaultToken(Integer id) {
        TinkoffDefaultToken token = getOne(id);
        tinkoffDefaultTokenRepository.delete(token);
    }
    public String getDefaultToken(){
        TinkoffDefaultToken defaultToken = getDefaultsToken().stream().findFirst().orElseThrow(
                () -> new EntityNotFoundException("Empty token list ")
        );

        return defaultToken.getToken();
    }

    @Override
    public TinkoffDefaultToken updateDefaultToken(String token) {
        TinkoffDefaultToken updatedToken = getDefaultsToken().stream().findFirst().orElseThrow(
                () -> new EntityNotFoundException("Empty token list ")
        );
        updatedToken.setToken(token);
        return tinkoffDefaultTokenRepository.save(updatedToken);
    }

    @Override
    public TinkoffDefaultToken addDefaultToken(String token) {
        List<TinkoffDefaultToken> tokens = getDefaultsToken();
        if (tokens.isEmpty()) {
            TinkoffDefaultToken tinkoffDefaultToken = new TinkoffDefaultToken();
            tinkoffDefaultToken.setId(1);
            tinkoffDefaultToken.setToken(token);
            return tinkoffDefaultTokenRepository.save(tinkoffDefaultToken);
        } else {
            return updateDefaultToken(token);
        }
    }
}
