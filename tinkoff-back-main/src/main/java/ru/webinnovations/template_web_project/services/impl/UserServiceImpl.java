package ru.webinnovations.template_web_project.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.webinnovations.template_web_project.configs.ContextProvider;
import ru.webinnovations.template_web_project.db.models.Role;
import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.db.repository.RoleRepository;
import ru.webinnovations.template_web_project.db.repository.UserRepository;
import ru.webinnovations.template_web_project.dto.UserDto;
import ru.webinnovations.template_web_project.dto.requests.LoginPasswordPut;
import ru.webinnovations.template_web_project.services.UserService;
import ru.webinnovations.template_web_project.services.mapper.UserMapper;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    private final UserMapper userMapper;

    private final ContextProvider contextProvider;
    private final TinkoffDefaultTokenServiceImpl tinkoffDefaultToken;


    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder,
                           UserMapper userMapper, ContextProvider contextProvider,
                           TinkoffDefaultTokenServiceImpl tinkoffDefaultToken) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
        this.contextProvider = contextProvider;
        this.tinkoffDefaultToken = tinkoffDefaultToken;
    }


    @Override
    public User register(User user) {
        Optional<Role> role = roleRepository.findByName("ROLE_USER");

        if (userRepository.findByUsername(user.getUsername()) != null) {
            throw new EntityExistsException("This login already exists!");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(role.orElseThrow(() -> new EntityNotFoundException("Not found role by name ROLE_USER")));

        return userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("Not found user by username " + username);
        }
        return user;
    }

    @Override
    public User findById(Long id) {
        User result = userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found user with id " + id));

        log.info("IN findById - user: {} found by id: {}", result, id);
        return result;
    }

    @Override
    public UserDto setTinkoffToken(String token) {
        tinkoffDefaultToken.addDefaultToken(token);
        return updateToken(token);
    }

    @Override
    public UserDto updateTinkoffToken(String newToken) {
        tinkoffDefaultToken.updateDefaultToken(newToken);
        return updateToken(newToken);
    }

    private UserDto updateToken(String token){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = findByUsername(authentication.getName());
        user.setToken(token);
        user.setIsTokenCreated(true);
        contextProvider.resetInvestApi();
        contextProvider.getInvestApi(user);
        return userMapper.toDto(userRepository.save(user));
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserDto updateLoginPassword(LoginPasswordPut request) {
        User user = findByUsername(request.getCurrentLogin());
        if(request.getNewLogin() != null && !request.getNewLogin().isBlank()){
            user.setUsername(request.getNewLogin());
        }
        if(request.getNewPassword() != null && !request.getNewPassword().isBlank()){
            user.setPassword(passwordEncoder.encode(request.getNewPassword()));
        }
        return userMapper.toDto(userRepository.save(user));
    }
}
