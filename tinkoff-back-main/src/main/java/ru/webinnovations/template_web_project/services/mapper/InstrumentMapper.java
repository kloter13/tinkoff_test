package ru.webinnovations.template_web_project.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.tinkoff.piapi.contract.v1.*;
import ru.webinnovations.template_web_project.db.enums.InstrumentTypeEnum;
import ru.webinnovations.template_web_project.db.models.Instrument;
import ru.webinnovations.template_web_project.dto.InstrumentDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class InstrumentMapper {

    private final ModelMapper modelMapper;

    public InstrumentMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    // Метод для маппинга сущности в DTO
    public InstrumentDTO toDto(Instrument instrument) {
        return modelMapper.map(instrument, InstrumentDTO.class);
    }

    public List<InstrumentDTO> toDto(List<Instrument> instruments) {
        List<InstrumentDTO> result = new ArrayList<>();

        for (Instrument instrument : instruments) {
            result.add(toDto(instrument));
        }

        return result;
    }

    // Метод для маппинга DTO в сущность
    public Instrument toEntity(InstrumentDTO instrumentDTO) {
        return modelMapper.map(instrumentDTO, Instrument.class);
    }

    public List<Instrument> toEntity(List<InstrumentDTO> instrumentDTOS) {
        List<Instrument> result = new ArrayList<>();

        for (InstrumentDTO instrumentDTO : instrumentDTOS) {
            result.add(toEntity(instrumentDTO));
        }

        return result;
    }

    public InstrumentDTO mapFromBond(Bond bond) {
        InstrumentDTO result = new InstrumentDTO();

        result.setType(InstrumentTypeEnum.BOND);
        result.setUid(bond.getUid());
        result.setPositionUid(bond.getPositionUid());
        result.setTicker(bond.getTicker());
        result.setLot(String.valueOf(bond.getLot()));
        result.setIsin(bond.getIsin());
        result.setFigi(bond.getFigi());
        result.setName(bond.getName());
        result.setCurrency(bond.getCurrency());
        result.setMinPriceIncrement(bond.getMinPriceIncrement().getNano()
                + "." + bond.getMinPriceIncrement().getUnits());
        result.setNominal(bond.getNominal().getNano()
                + "." + bond.getNominal().getUnits());
        result.setMinPriceIncrementAmount(bond.getMinPriceIncrement().getNano()
                + "." + bond.getMinPriceIncrement().getUnits());

        return result;
    }

    public InstrumentDTO mapFromShare(Share share) {
        InstrumentDTO result = new InstrumentDTO();

        result.setType(InstrumentTypeEnum.SHARE);
        result.setUid(share.getUid());
        result.setPositionUid(share.getPositionUid());
        result.setTicker(share.getTicker());
        result.setLot(String.valueOf(share.getLot()));
        result.setIsin(share.getIsin());
        result.setFigi(share.getFigi());
        result.setName(share.getName());
        result.setCurrency(share.getCurrency());
        result.setMinPriceIncrement(share.getMinPriceIncrement().getNano()
                + "." + share.getMinPriceIncrement().getUnits());
        result.setNominal(share.getNominal().getNano()
                + "." + share.getNominal().getUnits());
        result.setMinPriceIncrementAmount(share.getMinPriceIncrement().getNano()
                + "." + share.getMinPriceIncrement().getUnits());

        return result;
    }

    public InstrumentDTO mapFromCurrency(Currency currency) {
        InstrumentDTO result = new InstrumentDTO();

        result.setType(InstrumentTypeEnum.CURRENCY);
        result.setUid(currency.getUid());
        result.setPositionUid(currency.getPositionUid());
        result.setTicker(currency.getTicker());
        result.setLot(String.valueOf(currency.getLot()));
        result.setIsin(currency.getIsin());
        result.setFigi(currency.getFigi());
        result.setName(currency.getName());
        result.setCurrency(currency.getCurrency());
        result.setMinPriceIncrement(currency.getMinPriceIncrement().getNano()
                + "." + currency.getMinPriceIncrement().getUnits());
        result.setNominal(currency.getNominal().getNano()
                + "." + currency.getNominal().getUnits());
        result.setMinPriceIncrementAmount(currency.getMinPriceIncrement().getNano()
                + "." + currency.getMinPriceIncrement().getUnits());

        return result;
    }

    public InstrumentDTO mapFromEtf(Etf etf) {
        InstrumentDTO result = new InstrumentDTO();

        result.setType(InstrumentTypeEnum.ETF);
        result.setUid(etf.getUid());
        result.setPositionUid(etf.getPositionUid());
        result.setTicker(etf.getTicker());
        result.setLot(String.valueOf(etf.getLot()));
        result.setIsin(etf.getIsin());
        result.setFigi(etf.getFigi());
        result.setName(etf.getName());
        result.setCurrency(etf.getCurrency());
        result.setMinPriceIncrement(etf.getMinPriceIncrement().getNano()
                + "." + etf.getMinPriceIncrement().getUnits());
//        result.setNominal(etf.getNominal().getNano()
//                + "." + etf.getNominal().getUnits());
        result.setMinPriceIncrementAmount(etf.getMinPriceIncrement().getNano()
                + "." + etf.getMinPriceIncrement().getUnits());

        return result;
    }

    public InstrumentDTO mapFromFuture(Future future) {
        InstrumentDTO result = new InstrumentDTO();

        result.setType(InstrumentTypeEnum.FUTURE);
        result.setUid(future.getUid());
        result.setPositionUid(future.getPositionUid());
        result.setTicker(future.getTicker());
        result.setLot(String.valueOf(future.getLot()));
        //result.setIsin(future.getIsin());
        result.setFigi(future.getFigi());
        result.setName(future.getName());
        result.setCurrency(future.getCurrency());
        result.setMinPriceIncrement(future.getMinPriceIncrement().getNano()
                + "." + future.getMinPriceIncrement().getUnits());
//        result.setNominal(future.getNominal().getNano()
//                + "." + future.getNominal().getUnits());
        result.setMinPriceIncrementAmount(future.getMinPriceIncrement().getNano()
                + "." + future.getMinPriceIncrement().getUnits());

        return result;
    }

    public InstrumentDTO mapFromOption(Option option) {
        InstrumentDTO result = new InstrumentDTO();

        result.setType(InstrumentTypeEnum.OPTION);
        result.setUid(option.getUid());
        result.setPositionUid(option.getPositionUid());
        result.setTicker(option.getTicker());
        result.setLot(String.valueOf(option.getLot()));
//        result.setIsin(option.getIsin());
//        result.setFigi(option.getFigi());
        result.setName(option.getName());
        result.setCurrency(option.getCurrency());
        result.setMinPriceIncrement(option.getMinPriceIncrement().getNano()
                + "." + option.getMinPriceIncrement().getUnits());
//        result.setNominal(option.getNominal().getNano()
//                + "." + option.getNominal().getUnits());
        result.setMinPriceIncrementAmount(option.getMinPriceIncrement().getNano()
                + "." + option.getMinPriceIncrement().getUnits());

        return result;
    }
}
