package ru.webinnovations.template_web_project.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.webinnovations.template_web_project.db.models.Role;
import ru.webinnovations.template_web_project.dto.RoleDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class RoleMapper {

    private final ModelMapper modelMapper;

    public RoleMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public RoleDto toDto(Role role) {
        return modelMapper.map(role, RoleDto.class);
    }

    public List<RoleDto> toDto(List<Role> roles) {
        List<RoleDto> result = new ArrayList<>();

        for (Role role : roles) {
            result.add(toDto(role));
        }

        return result;
    }

    public Role toEntity(RoleDto roleDto) {
        return modelMapper.map(roleDto, Role.class);
    }

    public List<Role> toEntity(List<RoleDto> roleDtos) {
        List<Role> result = new ArrayList<>();

        for (RoleDto roleDto : roleDtos) {
            result.add(toEntity(roleDto));
        }

        return result;
    }
}
