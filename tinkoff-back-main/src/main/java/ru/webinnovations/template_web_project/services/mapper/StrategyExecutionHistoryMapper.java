package ru.webinnovations.template_web_project.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;
import ru.webinnovations.template_web_project.dto.StrategyExecutionHistoryDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class StrategyExecutionHistoryMapper {

    private final ModelMapper modelMapper;

    public StrategyExecutionHistoryMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public StrategyExecutionHistoryDTO toDto(StrategyExecutionHistory history) {
        return modelMapper.map(history, StrategyExecutionHistoryDTO.class);
    }

    public StrategyExecutionHistory toEntity(StrategyExecutionHistoryDTO dto) {
        return modelMapper.map(dto, StrategyExecutionHistory.class);
    }

    public List<StrategyExecutionHistoryDTO> toDto(List<StrategyExecutionHistory> histories) {
        List<StrategyExecutionHistoryDTO> result = new ArrayList<>();

        for (StrategyExecutionHistory history : histories) {
            result.add(toDto(history));
        }

        return result;
    }

    public List<StrategyExecutionHistory> toEntity(List<StrategyExecutionHistoryDTO> dtos) {
        List<StrategyExecutionHistory> result = new ArrayList<>();

        for (StrategyExecutionHistoryDTO dto : dtos) {
            result.add(toEntity(dto));
        }

        return result;
    }


}
