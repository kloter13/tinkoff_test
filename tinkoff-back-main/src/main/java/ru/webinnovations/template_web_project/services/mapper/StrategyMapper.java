package ru.webinnovations.template_web_project.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.dto.StrategyDTO;
import ru.webinnovations.template_web_project.utils.StrategyUtil;

import java.util.ArrayList;
import java.util.List;

@Component
public class StrategyMapper {

    private final ModelMapper modelMapper;
    private final InstrumentMapper instrumentMapper;
    private final StrategyExecutionHistoryMapper historyMapper;
    private final StrategyUtil strategyUtil;

    public StrategyMapper(ModelMapper modelMapper, InstrumentMapper instrumentMapper,
                          StrategyExecutionHistoryMapper historyMapper, StrategyUtil strategyUtil) {
        this.modelMapper = modelMapper;
        this.instrumentMapper = instrumentMapper;
        this.historyMapper = historyMapper;
        this.strategyUtil = strategyUtil;
    }

    public StrategyDTO toDto(Strategy strategy) {
        strategy.setExecutedOperation(Math.toIntExact(strategyUtil.getTotalExecutedLots(strategy)));
        StrategyDTO strategyDTO = modelMapper.map(strategy, StrategyDTO.class);
        strategyDTO.setInstrumentDTO(instrumentMapper.toDto(strategy.getInstrument()));
        if (strategy.getHistory() != null) {
            strategyDTO.setHistory(historyMapper.toDto(strategy.getHistory()));
        }
        return strategyDTO;
    }


    public List<StrategyDTO> toDto(List<Strategy> strategies) {
        List<StrategyDTO> result = new ArrayList<>();

        for (Strategy strategy : strategies) {
            result.add(toDto(strategy));
        }

        return result;
    }

    public Strategy toEntity(StrategyDTO strategyDTO) {
        Strategy strategy = modelMapper.map(strategyDTO, Strategy.class);
        strategy.setInstrument(instrumentMapper.toEntity(strategyDTO.getInstrumentDTO()));
        if (strategyDTO.getHistory() != null) {
            strategy.setHistory(historyMapper.toEntity(strategyDTO.getHistory()));
        }
        return strategy;
    }


    public List<Strategy> toEntity(List<StrategyDTO> strategyDTOS) {
        List<Strategy> result = new ArrayList<>();

        for (StrategyDTO strategyDTO : strategyDTOS) {
            result.add(toEntity(strategyDTO));
        }

        return result;
    }
}


