package ru.webinnovations.template_web_project.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.webinnovations.template_web_project.db.models.User;
import ru.webinnovations.template_web_project.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    private final ModelMapper modelMapper;
    private final RoleMapper roleMapper;
    private final StrategyMapper strategyMapper;

    public UserMapper(ModelMapper modelMapper, RoleMapper roleMapper, StrategyMapper strategyMapper) {
        this.roleMapper = roleMapper;
        this.modelMapper = modelMapper;
        this.strategyMapper = strategyMapper;
    }

    public UserDto toDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userDto.setRole(roleMapper.toDto(user.getRole()));
        if (userDto.getStrategies() != null) {
            userDto.setStrategies(strategyMapper.toDto(user.getStrategies()));
        }

        return userDto;
    }

    public List<UserDto> toDto(List<User> users) {
        List<UserDto> result = new ArrayList<>();

        for (User user : users) {
            result.add(toDto(user));
        }

        return result;
    }

    public User toEntity(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        user.setRole(roleMapper.toEntity(userDto.getRole()));
        if (user.getStrategies() != null) {
            user.setStrategies(strategyMapper.toEntity(userDto.getStrategies()));
        }

        return user;
    }

    public List<User> toEntity(List<UserDto> userDtos) {
        List<User> result = new ArrayList<>();

        for (UserDto userDto : userDtos) {
            result.add(toEntity(userDto));
        }

        return result;
    }


}
