package ru.webinnovations.template_web_project.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public final class RegexProvider {
    private static final String PATH_TO_PROPERTIES_FILE = "src/main/resources/regular_expressions.properties";
    private static final String NONE_MESSAGE = "None";
    private static final String ERROR_MESSAGE = "Error!";

    private RegexProvider() {
    }

    public static String get(String key) {
        FileInputStream fileInputStream;
        Properties properties = new Properties();
        String regex = NONE_MESSAGE;

        try {
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES_FILE);
            properties.load(fileInputStream);
            regex = properties.getProperty(key);
        } catch (IOException e) {
            log.error(ERROR_MESSAGE);
        }
        return regex;
    }

}
