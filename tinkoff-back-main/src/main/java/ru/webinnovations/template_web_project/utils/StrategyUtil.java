package ru.webinnovations.template_web_project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tinkoff.piapi.contract.v1.MoneyValue;
import ru.tinkoff.piapi.contract.v1.OrderState;
import ru.tinkoff.piapi.core.InvestApi;
import ru.webinnovations.template_web_project.configs.ContextProvider;
import ru.webinnovations.template_web_project.db.enums.StrategyOrderStatus;
import ru.webinnovations.template_web_project.db.models.MoneyValueModel;
import ru.webinnovations.template_web_project.db.models.OrderStateModel;
import ru.webinnovations.template_web_project.db.models.Strategy;
import ru.webinnovations.template_web_project.db.models.StrategyExecutionHistory;
import ru.webinnovations.template_web_project.services.StrategyExecutionHistoryService;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

@Component
public class StrategyUtil {
    private final ContextProvider contextProvider;
    private final StrategyExecutionHistoryService strategyExecutionHistoryService;

    @Autowired
    public StrategyUtil(ContextProvider contextProvider,
                        StrategyExecutionHistoryService strategyExecutionHistoryService) {
        this.contextProvider = contextProvider;
        this.strategyExecutionHistoryService = strategyExecutionHistoryService;
    }

    public InvestApi getInvestApi() {
        try {
            return contextProvider.getInvestApi();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean existsStrategyHistories(Strategy strategy) {
        return strategy.getHistory() != null && !strategy.getHistory().isEmpty();
    }

    public long getExecutedLots(String orderId) {
        StrategyExecutionHistory history = strategyExecutionHistoryService.getByOrderId(orderId);
        if (isCompletedOrder(history)) {
            return history.getVolume();
        }
        return getOrderState(orderId).getLotsExecuted();
    }

    private boolean isCompletedOrder(StrategyExecutionHistory history) {
        return history.getStatus().equals(StrategyOrderStatus.EXECUTION_REPORT_STATUS_FILL);
    }

    public long getTotalExecutedLots(Strategy strategy) {
        var result = 0L;
        if (existsStrategyHistories(strategy)) {
            var histories = strategy.getHistory();
            for (StrategyExecutionHistory history : histories) {
                result = result + getExecutedLots(history.getOrderId());
            }
        }
        return result;
    }

    public OrderStateModel getOrderState(String orderId) {
        InvestApi api = getInvestApi();
        var mainAccount = api.getUserService().getAccountsSync().get(0).getId();
        OrderState request = null;
        try {
            request = api.getOrdersService().getOrderState(mainAccount, orderId).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        return OrderStateModel.builder()
                .orderExecutionReportStatus(request.getExecutionReportStatus().name())
                .lotsRequested(request.getLotsRequested())
                .lotsExecuted(request.getLotsExecuted())
                .initialOrderPrice(convertMoneyValue(request.getInitialOrderPrice()))
                .executedOrderPrice(convertMoneyValue(request.getExecutedOrderPrice()))
                .totalOrderAmount(convertMoneyValue(request.getTotalOrderAmount()))
                .initialCommission(convertMoneyValue(request.getInitialCommission()))
                .executedCommission(convertMoneyValue(request.getExecutedCommission()))
                .orderId(orderId)
                .build();

    }

    public MoneyValueModel convertMoneyValue(MoneyValue moneyValue) {
        return MoneyValueModel
                .builder()
                .value(BigDecimal.valueOf(
                        moneyValue.getUnits()
                ).add(new BigDecimal("0.".concat(String.valueOf(moneyValue.getNano())))))
                .currency(moneyValue.getCurrency())
                .build();
    }
}
