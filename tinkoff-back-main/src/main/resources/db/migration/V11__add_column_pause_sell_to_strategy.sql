ALTER TABLE strategy
    ADD COLUMN if NOT EXISTS pause_sell bool DEFAULT FALSE;