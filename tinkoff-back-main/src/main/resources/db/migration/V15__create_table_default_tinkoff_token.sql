CREATE TABLE tinkoff_default_token
(
    id    INTEGER PRIMARY KEY,
    token VARCHAR(255)
);