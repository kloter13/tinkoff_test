CREATE TABLE instrument (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    type VARCHAR(100),
    uid VARCHAR(100),
    position_uid VARCHAR(100),
    figi VARCHAR(100),
    ticker VARCHAR(100),
    isin VARCHAR(100),
    lot VARCHAR(100),
    currency VARCHAR(100),
    nominal_in_currency VARCHAR(100),
    price VARCHAR(100),
    last_price VARCHAR(100),
    nominal VARCHAR(100),
    min_price_increment VARCHAR(100),
    min_price_increment_amount VARCHAR(100)
);
