CREATE TABLE strategy (
    id SERIAL PRIMARY KEY,
    volume BIGINT,
    buy_price VARCHAR(255),
    sell_price VARCHAR(255),
    date_of_begin TIMESTAMP,
    date_of_end TIMESTAMP,
    instrument_id BIGINT,
    FOREIGN KEY (instrument_id) REFERENCES instrument(id)
);
