CREATE TABLE strategy_execution_history (
    id SERIAL PRIMARY KEY,
    order_id VARCHAR(255) NOT NULL,
    strategy_id BIGINT NOT NULL,
    execution_timestamp TIMESTAMP,
    volume BIGINT,
    type VARCHAR(50),
    status VARCHAR(50),
    FOREIGN KEY (strategy_id) REFERENCES strategy(id)
);
