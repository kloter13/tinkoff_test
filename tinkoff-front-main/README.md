# Tinkoff-invest-UI

```sh
npm install

yarn install
```

### Compile and Hot-Reload for Development

```sh
npm run dev

yarn dev
```

### Compile and Minify for Production

```sh
npm run build

yarn build
```

#### First you need to create *.env_web_db* file and copy data from *.env_web_db.example* to the new file, then change POSTGRES password from the one

### Local build project
```bash
make local_build
```

### Local run project
```bash
make local_up
```

### Remove the containers
```bash
make local_down
```