import axios from 'axios'
import { VITE_APP_URL } from '../../config'

const Http = axios.create({
  baseURL: VITE_APP_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }
})

Http.interceptors.request.use((config) => {
  let localUser = JSON.parse(localStorage.getItem('tink-token') || null)

  if (localUser !== null) {
    config.headers.Authorization = `Bearer ${localUser.access_token}`
    return config
  } else {
    return config
  }
})

export default Http
