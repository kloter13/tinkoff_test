import './assets/css/tailwind.css'
import CanvasJSStockChart from '@canvasjs/vue-stockcharts'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex/store'
const app = createApp(App)

app.use(router)
app.use(store)
app.use(CanvasJSStockChart)
app.mount('#app')
