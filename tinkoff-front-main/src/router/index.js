import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../pages/HomePage.vue'
import NotFoundPage from '../pages/NotFoundPage.vue'
import { SecurityRoutes } from '../services/security/router'
import { StrategyRoutes } from '../services/strategy/router'
import NavbarComponent from '../components/NavbarComponent.vue'

const routes = [
  {
    path: '/',
    name: 'HomePage',
    components: { default: HomePage, header: NavbarComponent },
    meta: {
      PageTitle: 'Тинькофф-инвестиции'
    }
  },

  ...SecurityRoutes,

  ...StrategyRoutes,

  {
    path: '/404',
    alias: '/:pathMatch(.*)*',
    name: 'NotFoundPage',
    components: { default: NotFoundPage },
    meta: {
      PageTitle: 'Страница не найдена'
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.PageTitle}`
  const loggedIn = localStorage.getItem('tink-token')
  if (to.meta.authenticated && !loggedIn) {
    next({ name: 'LoginPage' })
  } else {
    next()
  }
})

export default router
