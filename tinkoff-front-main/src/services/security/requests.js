import { API } from '../../http/api'

const serviceBasePath = '/login'

export const SecurityRequests = {
  postRegisterUser(payload) {
    return API.makePostRequest(`${serviceBasePath}/register`, payload)
  },

  postLogin(payload) {
    return API.makePostRequest(`${serviceBasePath}/auth`, payload)
  },

  getUserMe() {
    return API.makeGetRequest(`users/me`)
  },

  postSetServiceToken(token) {
    return API.makePostRequest(`users/token`, token)
  },

  putSetServiceToken(token) {
    return API.makePutRequest(`users/token`, token)
  },

  putUserCredentials(payload) {
    return API.makePutRequest(`${serviceBasePath}/update`, payload)
  }
}
