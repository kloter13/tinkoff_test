import LoginPage from './pages/LoginPage.vue'
import NavbarComponent from '../../components/NavbarComponent.vue'
import PersonalAccountPage from './pages/PersonalAccountPage.vue'
import AddTokenPage from './pages/AddTokenPage.vue'

export const SecurityRoutes = [
  {
    path: '/login',
    name: 'LoginPage',
    components: { default: LoginPage },
    meta: {
      PageTitle: 'Авторизация',
      authenticated: false
    }
  },
  {
    path: '/account',
    name: 'PersonalAccountPage',
    components: { default: PersonalAccountPage, header: NavbarComponent },
    meta: {
      authenticated: true,
      PageTitle: 'Личный кабинет'
    }
  },
  {
    path: '/token',
    name: 'AddTokenPage',
    components: { default: AddTokenPage, header: NavbarComponent },

    meta: {
      authenticated: true,
      PageTitle: 'Токен подключения'
    }
  }
]
/*
 * vue vue@2
 * */
