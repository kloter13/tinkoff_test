import { API } from '../../../http/api'

const serviceBasePath = '/instrument'

export const InstrumentsRequests = {
  getAllInstruments: (payload) => {
    let query = payload?.params ? `?${payload?.params}` : ''

    return API.makeGetRequest(`${serviceBasePath}${query}`)
  },
  getInstrumentWithCables: (payload) => {
    let query = payload?.params ? `?${payload?.params}` : ''

    return API.makeGetRequest(`${serviceBasePath}/withCandles${query}`)
  },
  getInstrumentBalances: () => {
    return API.makeGetRequest(`${serviceBasePath}/portfolio`)
  }
}
