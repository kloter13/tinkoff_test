import { API } from '../../../http/api'

const serviceBasePath = '/strategy'

export const StrategiesRequests = {
  getAllStrategy() {
    return API.makeGetRequest(`${serviceBasePath}`)
  },

  getStrategyById(id) {
    return API.makeGetRequest(`${serviceBasePath}/${id}`)
  },
  getStrategyState(id) {
    return API.makeGetRequest(`${serviceBasePath}/state/${id}`)
  },

  getStrategyByUser() {
    return API.makeGetRequest(`${serviceBasePath}/byUser`)
  },

  postStrategy(payload) {
    return API.makePostRequest(`${serviceBasePath}`, payload)
  },

  putStrategy(payload) {
    return API.makePutRequest(`${serviceBasePath}`, payload)
  },

  deleteStrategy(id) {
    return API.makeDeleteRequest(`${serviceBasePath}?id=${id}`)
  }
}
