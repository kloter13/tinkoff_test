import NavbarComponent from '@/components/NavbarComponent.vue'
import NotFoundPage from '../../pages/NotFoundPage.vue'
import MainStrategyPage from './pages/MainStrategyPage.vue'

export const StrategyRoutes = [
  {
    path: '/strategies',
    name: 'MainOperationsView',
    components: { default: MainStrategyPage, header: NavbarComponent },
    children: [
      {
        path: '',
        alias: 'list',
        name: 'StrategyListPage',
        component: () => import('./pages/StrategyListPage.vue'),
        meta: {
          PageTitle: 'Операции'
        }
      },
      {
        path: 'create',
        name: 'CreateStrategyPage',
        component: () => import('./pages/CreateStrategyPage.vue'),
        meta: {
          PageTitle: 'Создание новой операции'
        }
      },
      {
        path: ':id',
        name: 'StrategyPage',
        component: () => import('./pages/StrategyPage.vue'),
        meta: {
          PageTitle: 'Просмотр Операции'
        }
      },
      {
        path: ':id/state',
        name: 'StrategyStatePage',
        component: () => import('./pages/StrategyStatePage.vue'),
        meta: {
          PageTitle: 'Просмотр Операции'
        }
      },
      {
        path: 'overview',
        name: 'StrategyOverviewPage',
        component: () => import('./pages/StrategyOverviewPage.vue'),
        meta: {
          PageTitle: 'Рабочий стол'
        }
      },
      {
        path: '404',
        alias: ':pathMatch(.*)*',
        name: 'NotFoundPage',
        components: { default: NotFoundPage },
        meta: {
          PageTitle: 'Страница не найдена'
        }
      }
    ],
    /* beforeEnter: () => {
      let serviceToken = getCredentials('is_service_token_added')
      let userToken = getCredentials('access_token')
      if (userToken && !serviceToken) {
        return { name: 'AddTokenPage' }
      }
    },*/
    meta: {
      authenticated: true,
      PageTitle: 'Рабочий стол'
    }
  },
  {
    path: '/404',
    alias: '/:pathMatch(.*)*',
    name: 'NotFoundPage',
    components: { default: NotFoundPage },
    meta: {
      PageTitle: 'Страница не найдена',
      authenticated: false
    }
  }
]
