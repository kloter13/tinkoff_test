import { InstrumentsRequests } from '../requests/instruments'
import store from '../../../vuex/store'
import { HandleErrorRequest } from '../../../use/useToast'
import { getChartData } from '../utils/useSerializedCandlesStrategyData'

export const InstrumentStore = {
  namespaced: true,

  state: {
    instruments: [],
    selectedInstrument: null,
    lastStrategy: {
      currency: 'string',
      figi: 'string',
      id: 0,
      isin: 'string',
      lastPrice: 'string',
      lastPriceList: [
        {
          closeNano: 23,
          closeUnits: 34,
          highNano: 1,
          highUnits: 56,
          isPriceComplete: true,
          lowNano: 23,
          lowUnits: 7,
          openNano: 67,
          openUnits: 9,
          timestamp: '2023-10-13T07:53:29.738Z'
        }
      ],
      lot: 'string',
      minPriceIncrement: 'string',
      minPriceIncrementAmount: 'string',
      name: 'string',
      nominal: 'string',
      nominalInCurrency: 'string',
      positionUid: 'string',
      price: 'string',
      ticker: 'string',
      type: 'BOND',
      uid: '224323fgdgf'
    },
    loading: false,
    loadingLastStrategy: false,
    overviewData: [],
    balances: {
      expectedYield: null,
      totalAmountBonds: {
        currency: 'rub',
        value: 0
      },
      totalAmountCurrencies: {
        currency: 'rub',
        value: 0
      },
      totalAmountEtf: {
        currency: 'rub',
        value: 0
      },
      totalAmountFutures: {
        currency: 'rub',
        value: 0
      },
      totalAmountShares: {
        currency: 'rub',
        value: 0
      }
    },
    dps1: [],
    dps3: []
  },
  getters: {
    dps1: (state) => {
      return state.dps1
    },
    dps3: (state) => {
      return state.dps3
    },
    balances: (state) => {
      return state.balances
    },
    overviewData: (state) => {
      return state.overviewData
    },
    selectedInstrument: (state) => {
      return state.selectedInstrument
    },
    loading: (state) => {
      return state.loading
    },
    instruments: (state) => {
      return state.instruments
    },
    loadingLastStrategy: (state) => {
      return state.loadingLastStrategy
    },
    lastStrategy: (state) => {
      return state.lastStrategy
    }
  },

  mutations: {
    PUSH_DPS1: (state, el) => {
      state.dps1.push(el)
    },
    PUSH_DPS3: (state, el) => {
      state.dps3.push(el)
    },
    SET_BALANCES: (state, balances) => {
      state.balances = balances
    },

    SET_INSTRUMENT: (state, instrument) => {
      state.selectedInstrument = instrument
      let serializedData = []
      if (instrument) {
        serializedData = getChartData(instrument.lastPriceList)
      }

      if (state.dps3.length) {
        state.dps3 = []
      }

      if (state.dps1.length) {
        state.dps1 = []
      }

      serializedData.forEach((data) => {
        state.dps1.push({
          x: new Date(data['date']),
          y: [data['open'], data['high'], data['low'], data['close']]
        })
        state.dps3.push({ x: new Date(data['date']), y: data['close'] })
      })
    },

    SET_LAST_INSTRUMENT: (state, lastStrategy) => {
      state.lastStrategy = lastStrategy
    },

    SET_INSTRUMENTS: (state, instruments) => {
      state.instruments = instruments
    },

    SET_LOADING: (state, loader) => {
      state.loading = loader
    },

    SET_LOADING_LAST_STRATEGY: (state, loader) => {
      state.loadingLastStrategy = loader
    }
  },

  actions: {
    // eslint-disable-next-line no-unused-vars
    async asyncAction({ commit }, callback) {
      callback()
    },

    getInstrumentBalances({ commit }) {
      InstrumentsRequests.getInstrumentBalances()
        .then((response) => {
          commit('SET_BALANCES', response.data)
        })
        .catch((error) => {
          console.error(error)
        })
    },

    getAllInstruments({ commit, state }, payload) {
      commit('SET_LOADING', true)
      InstrumentsRequests.getAllInstruments(payload)
        .then((response) => {
          commit('SET_LOADING', false)
          if (response.status === 200 && response.data?.content?.length) {
            if (payload.params !== null && state.selectedInstrument === null) {
              commit('SET_INSTRUMENT', response.data.content[0])
            }

            commit('SET_INSTRUMENTS', response.data.content)
            store.commit('SET_PAGEABLE', response.data)
          }
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          console.error(error)
        })
    },

    getInstrumentWithCables({ commit }, params) {
      if (params?.action === 'refresh') {
        commit('SET_LOADING_LAST_STRATEGY', true)
      } else {
        commit('SET_LOADING', true)
      }

      InstrumentsRequests.getInstrumentWithCables(params)
        .then((response) => {
          commit('SET_LOADING', false)
          commit('SET_LOADING_LAST_STRATEGY', false)
          if (response.status === 200 && response.data !== null) {
            if (Object.hasOwn(response.data, 'content')) {
              commit('SET_CHART_OVERVIEW_DATA', response?.data?.content)
              commit('SET_LAST_INSTRUMENT', response?.data?.content)
              commit('SET_INSTRUMENT', response?.data?.content?.lastPriceList)
            } else {
              store.commit('SET_CHART_OVERVIEW_DATA', response?.data?.lastPriceList)
              commit('SET_LAST_INSTRUMENT', response?.data)
              commit('SET_INSTRUMENT', response?.data)
            }
          }
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          commit('SET_LOADING_LAST_STRATEGY', false)
          HandleErrorRequest(
            {
              400: 'Время конца не может быть раньше Время начала',
              500: 'Укажите другой интервал'
            },
            error?.response?.status
          )
        })
    }
  }
}
