import { StrategiesRequests } from '../requests/strategies'
import router from '../../../router'
import { HandleErrorRequest, HandleSuccessRequest } from '../../../use/useToast'
import { strategyResponseCode } from '../utils/strategyResponseCode'
import store from '../../../vuex/store'
import { useHttpResponseHandler } from '../../../use/useHttpResponseHandler'

export const StrategyStore = {
  namespaced: true,

  state: {
    allStrategies: [],
    userStrategies: [],
    strategy: null,
    strategyState: null,
    loading: false,
    loadingStrategy: false
  },
  getters: {
    strategyState: (state) => {
      return state.strategyState
    },
    allStrategies: (state) => {
      return state.allStrategies
    },
    userStrategies: (state) => {
      return state.userStrategies
    },

    strategy: (state) => {
      return state.strategy
    },

    loading: (state) => {
      return state.loading
    },

    loadingStrategy: (state) => {
      return state.loadingStrategy
    }
  },

  mutations: {
    SET_STRATEGY_STATE_LOADING: (state, loadingStrategy) => {
      state.loadingStrategy = loadingStrategy
    },
    SET_STRATEGY_STATE: (state, strategyState) => {
      state.strategyState = strategyState
    },
    SET_ALL_STRATEGIES: (state, strategies) => {
      state.pollingSession = strategies
    },

    SET_USER_STRATEGIES: (state, userStrategies) => {
      state.userStrategies = userStrategies
    },

    PUSH_STRATEGY: (state, newOne) => {
      state.userStrategies.unshift(newOne)
    },

    SET_STRATEGY: (state, strategy) => {
      state.strategy = strategy
    },

    SET_LOADING: (state, loader) => {
      state.loading = loader
    }
  },

  actions: {
    getAllStrategy({ commit }) {
      commit('SET_LOADING', true)
      StrategiesRequests.getAllStrategy()
        .then((response) => {
          commit('SET_LOADING', false)

          if (response.status === 200) {
            commit('SET_ALL_STRATEGIES', response.data)
          }
        })
        .catch(() => {
          commit('SET_LOADING', false)
        })
    },

    getStrategyState({ commit }, id) {
      commit('SET_STRATEGY_STATE_LOADING', true)
      StrategiesRequests.getStrategyState(id)
        .then((response) => {
          commit('SET_STRATEGY_STATE_LOADING', false)

          if (response.status === 200) {
            commit('SET_STRATEGY_STATE', response.data)
          }
        })
        .catch((err) => {
          if (err?.response?.status === 500) {
            useHttpResponseHandler({ 500: 'Торговое поручение не найдено' }).errorRequest(500)
          }
          commit('SET_STRATEGY_STATE_LOADING', false)
        })
    },

    getStrategyById({ commit }, payload) {
      commit('SET_LOADING', true)
      StrategiesRequests.getStrategyById(payload.id)
        .then((response) => {
          commit('SET_LOADING', false)

          if (response.status === 200) {
            commit('SET_STRATEGY', response.data)

            if (payload.orderId === null && response.data?.history?.length > 0) {
              router
                .push({
                  name: 'StrategyStatePage',
                  query: { order_id: response.data?.history[0]?.orderId }
                })
                .then()
            }
          }
        })
        .catch(() => {
          router
            .push({
              name: 'StrategyListPage'
            })
            .then()
          commit('SET_LOADING', false)
        })
    },

    getStrategyByUser({ commit }, action) {
      if (action && action !== 'refresh') {
        commit('SET_LOADING', true)
      }
      StrategiesRequests.getStrategyByUser()
        .then((response) => {
          commit('SET_LOADING', false)

          if (response.status === 200) {
            commit('SET_USER_STRATEGIES', response.data)
          }
        })
        .catch(() => {
          commit('SET_LOADING', false)
        })
    },

    postStrategy({ commit }, payload) {
      commit('SET_LOADING', true)
      StrategiesRequests.postStrategy(payload)
        .then((response) => {
          commit('SET_LOADING', false)

          if (response.status === 200) {
            HandleSuccessRequest('Новая операция успешно создана')
            router.push({ name: 'StrategyListPage' }).then()
            store.dispatch('getMe').then()
          }
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          HandleErrorRequest(strategyResponseCode, error?.response?.status)
        })
    },

    putStrategy({ commit, dispatch }, payload) {
      commit('SET_LOADING', true)
      StrategiesRequests.putStrategy(payload)
        .then(() => {
          commit('SET_LOADING', false)
          HandleSuccessRequest('операция успешно обновлена')
          dispatch('getStrategyByUser', 'refresh')
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          HandleErrorRequest(strategyResponseCode, error?.response?.status)
        })
    },

    deleteStrategy({ commit }, payload) {
      commit('SET_LOADING', true)
      StrategiesRequests.deleteStrategy(payload)
        .then(() => {
          commit('SET_LOADING', false)
          HandleSuccessRequest('Операция успешно удалена')
          store.dispatch('userSession').then()
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          HandleErrorRequest(strategyResponseCode, error?.response?.status)
        })
    }
  }
}
