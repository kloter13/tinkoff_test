import { helpers, minValue, required } from '@vuelidate/validators'

export const strategyFormValidation = () => {
  return {
    strategyForm: {
      buyPrice: {
        required: helpers.withMessage('Максимальная цена не может быть пустой', required),
        minValue: minValue(0)
      },
      dateOfBegin: {
        required: helpers.withMessage('Обязательное поле', required)
      },
      orderType: {
        required: helpers.withMessage('Обязательное поле', required)
      },
      dateOfEnd: {
        required: helpers.withMessage('Обязательное поле', required)
      },

      sellPrice: {
        required: helpers.withMessage('Минимальная цена не может быть пустой', required),
        minValue: minValue(0)
      },
      volume: {
        required: helpers.withMessage('Объем не может быть пустой', required),
        minValue: minValue(0)
      }
    }
  }
}
