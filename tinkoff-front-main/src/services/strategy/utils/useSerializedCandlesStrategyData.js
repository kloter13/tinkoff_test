export const getChartData = (lastPriceList) => {
  if (lastPriceList === null) return []

  let serialisedData = []

  const getItem = (price) => {
    let date = price.timestamp.slice(0, 10)

    return {
      date: date,
      open: +(price.openUnits + '.' + price.openNano),
      high: +(price.highUnits + '.' + price.highNano),
      low: +(price.lowUnits + '.' + price.lowNano),
      close: +(price.closeUnits + '.' + price.closeNano)
    }
  }

  lastPriceList.forEach((price) => {
    serialisedData.push(getItem(price))
  })

  return serialisedData
}
