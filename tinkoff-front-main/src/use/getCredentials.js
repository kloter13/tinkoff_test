export const getCredentials = (field) => {
  const credentials = JSON.parse(localStorage.getItem('tink-token'))

  if (!credentials) return

  if (field) {
    return credentials[field]
  } else {
    return credentials
  }
}
