import {
  CANDLE_INTERVAL_10_MIN,
  CANDLE_INTERVAL_15_MIN,
  CANDLE_INTERVAL_1_MIN,
  CANDLE_INTERVAL_2_HOUR,
  CANDLE_INTERVAL_2_MIN,
  CANDLE_INTERVAL_30_MIN,
  CANDLE_INTERVAL_3_MIN,
  CANDLE_INTERVAL_4_HOUR,
  CANDLE_INTERVAL_5_MIN,
  CANDLE_INTERVAL_DAY,
  CANDLE_INTERVAL_HOUR,
  CANDLE_INTERVAL_MONTH,
  CANDLE_INTERVAL_UNSPECIFIED,
  CANDLE_INTERVAL_WEEK,
  UNRECOGNIZED
} from '../constants/conts'

export const candleList = {
  min1: Symbol(CANDLE_INTERVAL_1_MIN),
  min2: Symbol(CANDLE_INTERVAL_2_MIN),
  min3: Symbol(CANDLE_INTERVAL_3_MIN),
  min5: Symbol(CANDLE_INTERVAL_5_MIN),
  min10: Symbol(CANDLE_INTERVAL_10_MIN),
  min15: Symbol(CANDLE_INTERVAL_15_MIN),
  min30: Symbol(CANDLE_INTERVAL_30_MIN),
  hour1: Symbol(CANDLE_INTERVAL_HOUR),
  hour2: Symbol(CANDLE_INTERVAL_2_HOUR),
  hour4: Symbol(CANDLE_INTERVAL_4_HOUR),
  day1: Symbol(CANDLE_INTERVAL_DAY),
  week1: Symbol(CANDLE_INTERVAL_WEEK),
  month1: Symbol(CANDLE_INTERVAL_MONTH),
  unspecified: Symbol(CANDLE_INTERVAL_UNSPECIFIED),
  unrecognized: Symbol(UNRECOGNIZED)
}

export const useCandleInterval = (candle = candleList.oil.description) => {
  let value

  switch (candle) {
    case candleList.min1.description:
      value = '1 минута'
      break
    case candleList.min2.description:
      value = '2 минуты'
      break
    case candleList.min3.description:
      value = '3 минуты'
      break
    case candleList.min5.description:
      value = '5 минут'
      break
    case candleList.min10.description:
      value = '10 минут'
      break
    case candleList.min15.description:
      value = '15 минут'
      break
    case candleList.min30.description:
      value = '30 минут'
      break
    case candleList.hour1.description:
      value = '1 час'
      break
    case candleList.hour2.description:
      value = '2 часа'
      break
    case candleList.hour4.description:
      value = '4 часа'
      break
    case candleList.day1.description:
      value = '1 день'
      break
    case candleList.week1.description:
      value = '1 неделя'
      break
    case candleList.month1.description:
      value = '1 месяц'
      break
    case candleList.unspecified.description:
      value = 'неопределенный'
      break
    default:
      value = 'неизвестный'
  }

  return value
}
