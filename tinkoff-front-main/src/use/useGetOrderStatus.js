import { BUY, NONE, SELL } from '../constants/conts'

const orderStatusList = {
  sell: Symbol(SELL),
  buy: Symbol(BUY),
  None: Symbol(NONE)
}

export const useGetOrderStatus = (strategyStatus = orderStatusList.buy.description) => {
  let status

  switch (strategyStatus) {
    case orderStatusList.buy.description:
      status = 'покупка'
      break
    case orderStatusList.sell.description:
      status = 'продажа'
      break
    default:
      status = 'None'
  }

  return status
}
