import { EXPIRED, FAILURE, IN_PROGRESS, NONE, PARTLY, SUCCESS } from '../constants/conts'

const strategyStatusList = {
  success: Symbol(SUCCESS),
  in_progress: Symbol(IN_PROGRESS),
  failure: Symbol(FAILURE),
  partly: Symbol(PARTLY),
  expired: Symbol(EXPIRED),
  None: Symbol(NONE)
}

export const useGetStrategyStatus = (
  strategyStatus = strategyStatusList.in_progress.description
) => {
  let status

  switch (strategyStatus) {
    case strategyStatusList.success.description:
      status = 'Успешно'
      break
    case strategyStatusList.partly.description:
      status = 'Частично'
      break
    case strategyStatusList.expired.description:
      status = 'Истек'
      break
    case strategyStatusList.in_progress.description:
      status = 'В процессе'
      break
    case strategyStatusList.failure.description:
      status = 'неуспешный'
      break
    default:
      status = 'None'
  }

  return status
}
