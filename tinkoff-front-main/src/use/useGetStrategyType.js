import {
  ORDER_TYPE_BESTPRICE,
  ORDER_TYPE_LIMIT,
  ORDER_TYPE_MARKET,
  ORDER_TYPE_UNSPECIFIED
} from '../constants/conts'

export const orderTypesList = {
  unspecified: Symbol(ORDER_TYPE_UNSPECIFIED),
  limit: Symbol(ORDER_TYPE_LIMIT),
  market: Symbol(ORDER_TYPE_MARKET),
  bestPrice: Symbol(ORDER_TYPE_BESTPRICE)
}

export const useGetStrategyType = (orderType = orderTypesList.limit.description) => {
  let type

  switch (orderType) {
    case orderTypesList.unspecified.description:
      type = 'Значение не указано'
      break
    case orderTypesList.limit.description:
      type = 'Лимитная'
      break
    case orderTypesList.market.description:
      type = 'Рыночная'
      break
    case orderTypesList.bestPrice.description:
      type = 'Лучшая цена'
      break
    default:
      type = 'None'
  }

  return type
}
