import { GAS_WIDGET, OIL_WIDGET } from '../constants/conts'

const widgetsList = {
  oil: Symbol(OIL_WIDGET),
  gas: Symbol(GAS_WIDGET)
}

export const useGetWidget = (widgetStatus = widgetsList.oil.description) => {
  let status

  switch (widgetStatus) {
    case widgetsList.oil.description:
      status = 'Нефть'
      break
    case widgetsList.gas.description:
      status = 'Газ'
      break
    default:
      status = 'None'
  }

  return status
}
