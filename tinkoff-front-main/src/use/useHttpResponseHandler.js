import { ToastAlert } from './useToast'

export function useHttpResponseHandler(args) {
  const res = {
    401: 'Метод не разрешен',
    ...args
  }

  const errorRequest = (code) => {
    ToastAlert.errorAlertModal(_getCaseMessage(res)(code))
  }

  const successRequest = (message) => {
    ToastAlert.successAlertModal(message)
  }

  const _getCaseMessage =
    (allCases, defaultMessage = 'Неизвестная ошибка') =>
    (code) => {
      return allCases[code] || defaultMessage
    }

  return {
    successRequest,
    errorRequest
  }
}
