export const useObjectToParams = (filterForm) => {
  let searchParams = new URLSearchParams()
  Object.keys(filterForm).forEach((key) => {
    const value = filterForm[key]

    if (value !== null && value !== '') {
      searchParams.set(key, value)
    }
  })

  return searchParams.toString()
}
