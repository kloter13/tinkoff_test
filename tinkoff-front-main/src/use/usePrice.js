const usePrice = (price, currencyCode) => {
  /* Use currencyCode: 'USD' for Dollar*/
  return new Intl.NumberFormat('ru-RU', {
    style: 'currency',
    currency: currencyCode,
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
    trailingZeroDisplay: 'stripIfInteger'
  }).format(price)
}

export { usePrice }
