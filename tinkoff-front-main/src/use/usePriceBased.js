import {
  CLOSE_NANO,
  CLOSE_UNITS,
  HIGH_NANO,
  HIGH_UNITS,
  LOW_NANO,
  LOW_UNITS,
  OPEN_NANO,
  OPEN_UNITS
} from '../constants/conts'

const priceBasedList = {
  high_nano: Symbol(HIGH_NANO),
  low_nano: Symbol(LOW_NANO),
  close_nano: Symbol(CLOSE_NANO),
  open_nano: Symbol(OPEN_NANO),
  close_units: Symbol(CLOSE_UNITS),
  high_units: Symbol(HIGH_UNITS),
  low_units: Symbol(LOW_UNITS),
  open_units: Symbol(OPEN_UNITS)
}

export const usePriceBased = (priceBased = priceBasedList.high_nano.description) => {
  let option

  switch (priceBased) {
    case priceBasedList.high_nano.description:
      option = 'High'
      break
    case priceBasedList.low_nano.description:
      option = 'Low'
      break
    case priceBasedList.close_nano.description:
      option = 'Close'
      break
    case priceBasedList.open_nano.description:
      option = 'Open'
      break
    case priceBasedList.close_units.description:
      option = 'Close'
      break
    case priceBasedList.high_units.description:
      option = 'High'
      break
    case priceBasedList.low_units.description:
      option = 'Low'
      break
    case priceBasedList.open_units.description:
      option = 'Open'
      break
    default:
      option = 'None'
  }

  return option
}
