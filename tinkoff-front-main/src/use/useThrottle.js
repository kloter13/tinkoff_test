export function useThrottle(callback, canThrottle = true, delay) {
  let isWaiting = false;
  let savedArgs = null;
  let savedThis = null;

  if (canThrottle) {
    return function wrapper(...args) {
      if (isWaiting) {
        savedThis = this;
        savedArgs = args;
        return;
      }

      callback.apply(this, args);

      isWaiting = true;
      setTimeout(() => {
        isWaiting = false;
        wrapper.apply(savedThis, savedArgs);
      }, delay);
    };
  }
}
