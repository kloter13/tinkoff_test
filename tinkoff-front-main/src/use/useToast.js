import store from '../vuex/store'
import { useHttpResponseHandler } from './useHttpResponseHandler'

export const ToastAlert = {
  successAlertModal(text, time = 3000) {
    store
      .dispatch('Toast/toast', {
        text: text,
        time: time
      })
      .then()
  },

  errorAlertModal(text = 'Произошла ошибка') {
    store
      .dispatch('Toast/toast', {
        type: 'error',
        text: text
      })
      .then()
  }
}

export const HandleErrorRequest = (responsesCase, statusCode) => {
  return useHttpResponseHandler(responsesCase).errorRequest(statusCode)
}

export const HandleSuccessRequest = (message) => {
  return useHttpResponseHandler().successRequest(message)
}
