import { createStore } from 'vuex'
import { StrategyStore } from '../services/strategy/stores/strategy'
import { InstrumentStore } from '../services/strategy/stores/instrument'
import { SecurityRequests } from '../services/security/requests'
import router from '../router'
import { strategyResponseCode } from '../services/strategy/utils/strategyResponseCode'
import { securityResponsesCase } from '../services/security/utils/securityResponseCode'
import { getCredentials } from '../use/getCredentials'
import { widgetTypes } from '../constants/staticData'
import { getChartData } from '../services/strategy/utils/useSerializedCandlesStrategyData'
import { UseToast } from '../use/toast'
import { HandleErrorRequest, HandleSuccessRequest } from '../use/useToast'

const userData = {
  id: null,
  isTokenCreated: null,
  name: null,
  role: null,
  strategies: [],
  username: null
}
const userTokens = JSON.parse(localStorage.getItem('tink-token'))
const initialUserState = userTokens
  ? {
      status: { loggedIn: true },
      userInfo: {
        ...userData,
        isTokenCreated: userTokens?.is_service_token_added || null,
        token: userTokens?.access_token || null
      }
    }
  : {
      status: { loggedIn: false },
      userData
    }

export default createStore({
  state: {
    ...initialUserState,
    loading: false,
    pollingSession: null,
    isFetchingData: false,
    canPullSession: !!localStorage.getItem('tink-token'),
    ...widgetTypes,
    currentOperation: null,
    isDeleting: false,
    deleteModalForm: {
      open: false,
      confirmCallback: null,
      fullBtnLabel: null,
      rejectCallback: null,
      message: ''
    },
    lastVisitedPage: 0,
    pageableForm: {
      empty: null,
      size: 20,
      totalElements: null,
      totalPages: null,
      pageable: {
        sort: {
          sorted: false,
          unsorted: true,
          empty: true
        },
        pageNumber: null,
        pageSize: 20,
        offset: null,
        paged: true,
        unpaged: false
      }
    },
    overviewData: []
  },

  getters: {
    isFetchingData: (state) => {
      return state.isFetchingData
    },
    overviewData: (state) => {
      return state.overviewData
    },
    canPullSession: (state) => {
      return state.canPullSession
    },
    pollingSession: (state) => {
      return state.pollingSession
    },

    userInfo: (state) => {
      return state.userInfo
    },

    token: (state) => {
      return state.userInfo.token
    },

    loading: (state) => {
      return state.loading
    },
    lastVisitedPage: (state) => {
      return state.lastVisitedPage
    },
    pageableForm: (state) => {
      return state.pageableForm
    },
    deleteModalForm: (state) => {
      return state.deleteModalForm
    },

    isDeleting: (state) => {
      return state.isDeleting
    },
    operationsByMonth: (state) => {
      return state.operationsByMonth
    },

    widgetTypes: (state) => {
      return state.widgetTypes
    }
  },

  mutations: {
    PUSH_STRATEGY_TO_USER: (state, newOne) => {
      state.userInfo.strategies.unshift(newOne)
    },

    SET_SERVICE_TOKEN: (state) => {
      state.userInfo.isTokenCreated = true
      let localUserCredentials = getCredentials()
      if (localUserCredentials && !!localUserCredentials?.access_token) {
        localStorage.setItem(
          'tink-token',
          JSON.stringify({
            ...localUserCredentials,
            is_service_token_added: true
          })
        )
      }
    },

    SET_CHART_OVERVIEW_DATA: (state, lastPriceList) => {
      state.overviewData = getChartData(lastPriceList)
    },
    SET_LOADING: (state, loader) => {
      state.loading = loader
    },

    LOGIN_SUCCESS: (state, user) => {
      if (user) {
        state.status.loggedIn = true
        state.userInfo.username = user?.username
        state.userInfo.token = user?.token
        state.userInfo.isTokenCreated = user?.isTokenCreated

        localStorage.setItem(
          'tink-token',
          JSON.stringify({
            access_token: user.token,
            is_service_token_added: user.isTokenCreated
          })
        )
      }
    },

    SET_USER_INFO: (state, user) => {
      let localUserCredentials = getCredentials()

      if (localUserCredentials && !!localUserCredentials?.access_token) {
        state.userInfo = {
          ...user,
          token: localUserCredentials?.access_token
        }

        localStorage.setItem(
          'tink-token',
          JSON.stringify({
            ...localUserCredentials,
            is_service_token_added: user?.isTokenCreated
          })
        )
      }
    },

    LOGIN_FAILURE: (state) => {
      state.status.loggedIn = false
      state.userInfo = userData
    },

    RESET_AUTH: (state) => {
      state.status.loggedIn = false
      localStorage.removeItem('tink-token')
      state.userInfo = userData
    },

    PULL_SESSION: (state, value) => {
      if (value === null) {
        state.canPullSession = false
      }
      state.pollingSession = value
    },

    SET_OPEN_DELETE_MODAL: (state, form) => {
      state.deleteModalForm = form
    },

    SET_CURRENT_OPERATION: (state, operation) => {
      state.currentOperation = operation
    },

    SET_IS_DELETING: (state, status) => {
      state.isDeleting = status
    },
    SET_IS_FETCHING_DATA: (state, status) => {
      state.isFetchingData = status
    },
    SET_LAST_PAGE: (state, page) => {
      state.lastVisitedPage = page
    },

    SET_PAGEABLE: (state, pageableForm) => {
      state.pageableForm.empty = pageableForm?.empty
      state.pageableForm.size = pageableForm?.size
      state.pageableForm.pageable = pageableForm?.pageable
      state.pageableForm.totalElements = pageableForm?.totalElements
      state.pageableForm.totalPages = pageableForm?.totalPages
    }
  },

  actions: {
    async handleDeletionActions(context, { confirmCallback }) {
      confirmCallback()
    },

    setCurrentOption({ commit }, payload) {
      commit('SET_CURRENT_OPERATION', payload)
    },

    async userSession({ commit, dispatch, state }) {
      commit('SET_IS_FETCHING_DATA', true)
      if (state.userInfo.isTokenCreated) {
        SecurityRequests.getUserMe()
          .then((response) => {
            commit('SET_IS_FETCHING_DATA', false)
            commit('Strategies/SET_USER_STRATEGIES', response.data?.strategies || [])
          })
          .catch((error) => {
            commit('PULL_SESSION', null)

            if (error?.response?.status !== 500) {
              dispatch('logout').then()
            }
          })
      } else {
        commit('PULL_SESSION', null)
        commit('SET_IS_FETCHING_DATA', false)
      }
    },

    logout({ commit }) {
      commit('RESET_AUTH')
      router.push({ name: 'LoginPage' }).then()
    },

    registerUser({ commit }, credentials) {
      commit('SET_LOADING', true)
      SecurityRequests.postRegisterUser(credentials)
        .then((response) => {
          commit('SET_LOADING', false)

          if (response.status === 200) {
            HandleSuccessRequest('Пользователь успешно создан')
          }

          router.push({ name: 'LoginPage' }).then()
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          HandleErrorRequest(strategyResponseCode, error?.response?.status)
        })
    },

    login({ commit, dispatch }, credentials) {
      commit('SET_LOADING', true)
      SecurityRequests.postLogin(credentials)
        .then((response) => {
          commit('LOGIN_SUCCESS', response.data)
          commit('SET_LOADING', false)

          if (response.data?.isTokenCreated) {
            dispatch('getMe')
            router.push({ name: 'StrategyListPage' }).then()
          } else {
            router.push({ name: 'HomePage' }).then()
          }
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          commit('LOGIN_FAILURE')
          HandleErrorRequest(securityResponsesCase, error?.response?.status)
        })
    },

    setServiceToken({ commit, state }, payload) {
      commit('SET_LOADING', true)
      SecurityRequests[isPostOrUpdated()](payload)
        .then((response) => {
          commit('SET_LOADING', false)
          if (response.status === 200) {
            commit('SET_SERVICE_TOKEN')
            HandleSuccessRequest('Токен создан')
            router.push({ name: 'StrategyListPage' }).then()
          }
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          HandleErrorRequest(securityResponsesCase, error?.response?.status)
        })

      function isPostOrUpdated() {
        return state.userInfo.isTokenCreated ? 'putSetServiceToken' : 'postSetServiceToken'
      }
    },

    getMe({ commit }) {
      commit('SET_LOADING', true)
      SecurityRequests.getUserMe()
        .then((response) => {
          commit('SET_LOADING', false)
          if (response.status === 200) {
            commit('SET_USER_INFO', response.data)
            commit('Strategies/SET_USER_STRATEGIES', response.data?.strategies || [])
          }
        })
        .catch((error) => {
          commit('SET_LOADING', false)
          console.error(error)
        })
    }
  },

  modules: {
    Strategies: StrategyStore,
    Instruments: InstrumentStore,
    Toast: UseToast
  }
})
